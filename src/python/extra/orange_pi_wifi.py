"""


Source URL to manage thw Wifi or OrangePi
https://debian-facile.org/doc:reseau:interfaces:wifi

"""


import logging
import subprocess
from urllib.error import URLError
import urllib.request


_logger = None


class OrangePi(object):
    '''
    Classe utilise pour interragir avec Android
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)

    def is_wifi(self)-> bool:
        '''
        Detect si access point est la

        Si oui verification si internet est disponible

        Si non alors redemarrage networking ou test pour redemarrer seulement la carte wifi
        '''
        self._logger.debug("Start wifi analysis")
        reseau_dispo = False
        # Reseau local dispo
        if self._is_network_avail():
            reseau_dispo = True
        else:
            self._enable_network()
            reseau_dispo = self._is_network_avail()
        self._logger.debug("Network status:{}".format(reseau_dispo))
        return reseau_dispo

    def screen_off(self):
        pass

    def screen_on(self):
        pass
        
    def set_wifi_state(self, new_state:bool):
        is_network_avail = None
        if True==new_state:
            is_network_avail=self.is_wifi()
        else:
            self._enable_wifi(False)
            is_network_avail=self._is_network_avail()
        return is_network_avail

    def lock(self):
        # self._enable_wifi(False)
        pass

    def _enable_network(self):

        if self._verify_url('http://192.168.254.1'):
            self._restart_network()
        else:
            self._enable_wifi(True)
            if self.verify_wifi_status():
                if self._verify_url('http://192.168.254.1'):
                    self._restart_network()

    def _is_network_avail(self) -> bool:
        reseau_dispo = False
        # Reseau local dispo
        if self._verify_url('http://192.168.254.1'):
            # Internet dispo
            if self._verify_url('http://www.domoticz.com'):
                reseau_dispo = True
        return reseau_dispo

    def _enable_wifi(self, state: bool):
        if state:
            state_txt = "on"
        else:
            state_txt = "off"
        self._logger.debug(state_txt)
        cmd = ["nmcli", "radio", "wifi", state_txt]

        self._execute_cmd(cmd)

    def _restart_network(self):
        cmd = ["sudo", "systemctl", "restart", "networking"]
        self._execute_cmd(cmd)

    def verify_wifi_status(self)->bool:
        cmd = ["ip", "a", "show", "wlan0"]
        try:
            output = subprocess.check_output(cmd)

            print(output)
        except subprocess.CalledProcessError as e:
            print("Exception {}".format(e))
        return True

    def _verify_url(self, url: str) -> bool:
        last_comm_success = False
        try:
            with urllib.request.urlopen(url) as response:
                # html = response.read()
                if response.status != 200:
                    self._logger.error('com error')
                else:
                    last_comm_success = True

        except URLError as e:
            self._logger.debug("Exception URLError {}".format(e))
        except ConnectionRefusedError as e:
            self._logger.debug(
                "Exception ConnectionRefusedError {}".format(e))
        return last_comm_success

    def _execute_cmd(self, cmd):
        try:
            with subprocess.Popen(cmd) as proc:
                proc.communicate()
        except:
            # Ignore error to permit continue process and cases debug on non
            # android
            pass


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    _logger = logging.getLogger()
    opi = OrangePi()
    exec_net = opi.is_wifi()
    _logger.info("Exec network: {}".format(exec_net))
