'''
@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import unittest
from donnees.horaire_stm.manage_stm import ManageSTM
import datetime


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_horaire(self):
        stm = ManageSTM()
        stm.set_bus_stop("51176")
        stm.set_list_bus_number(["51"])
        stm.set_stm_data_dir("/home/soniahugo/.stmcli")
        stm.set_nombre_horaire(10)
        date_time = datetime.datetime(2018, 7, 6, 8, 15)
        """

        sqlitebrowser
        
        ('SELECT "t2"."departure_time" 
            FROM "trips" 
                AS t1 INNER JOIN "stop_times" 
                AS t2 ON ("t1"."trip_id" = "t2"."trip_id_id") INNER JOIN "stops" 
                AS t3 ON ("t2"."stop_id_id" = "t3"."stop_id") 
                    WHERE ((("t1"."route_id_id" = ?) 
                            AND ("t3"."stop_code" = ?)) 
                            AND ("t1"."service_id" 
                                 = (SELECT "t4"."service_id" 
                                    FROM "calendar_dates" AS t4 
                                    WHERE ("t4"."date" = ?)))) 
                    ORDER BY "t2"."departure_time"', ['51', '51176', '20180706'])

# Requete non fonctionnelle
SELECT "t2"."departure_time" 
            FROM "trips" 
                AS t1 INNER JOIN "stop_times" 
                AS t2 ON ("t1"."trip_id" = "t2"."trip_id_id") INNER JOIN "stops" 
                AS t3 ON ("t2"."stop_id_id" = "t3"."stop_id") 
                    WHERE ((("t1"."route_id_id" = '51') 
                            AND ("t3"."stop_code" = '51176')) 
                            AND ("t1"."service_id" 
                                 in (SELECT "t4"."service_id" 
                                    FROM "calendar_dates" AS t4 
                                    WHERE ("t4"."date" = '20180706')))) 
                    ORDER BY "t2"."departure_time";

SELECT "t2"."departure_time" 
            FROM "trips" 
                AS t1 INNER JOIN "stop_times" 
                AS t2 ON ("t1"."trip_id" = "t2"."trip_id_id") INNER JOIN "stops" 
                AS t3 ON ("t2"."stop_id_id" = "t3"."stop_id") 
                    WHERE ((("t1"."route_id_id" = '51') 
                            AND ("t3"."stop_code" = '51176')) 
                            AND ("t1"."service_id" 
                                 in (SELECT "t4"."service_id" 
                                    FROM "calendar_dates" AS t4 
                                    WHERE ("t4"."date" = '20180711')))) 
                    ORDER BY "t2"."departure_time";
SELECT "t2"."departure_time" 
            FROM "trips" 
                AS t1 INNER JOIN "stop_times" 
                AS t2 ON ("t1"."trip_id" = "t2"."trip_id_id") INNER JOIN "stops" 
                AS t3 ON ("t2"."stop_id_id" = "t3"."stop_id") 
                    WHERE ((("t1"."route_id_id" = '51') 
                            AND ("t3"."stop_code" = '51176')) 
                            AND ("t1"."service_id" 
                                 in (SELECT "t4"."service_id" 
                                    FROM "calendar_dates" AS t4 
                                    WHERE ("t4"."date" = '20180711')))) 
                    ORDER BY "t2"."departure_time";

SELECT "t4"."service_id" FROM "calendar_dates" AS t4 WHERE ("t4"."date" = '20180706');

# Requete avec résultat
SELECT "t2"."departure_time" 
            FROM "trips" 
                AS t1 INNER JOIN "stop_times" 
                AS t2 ON ("t1"."trip_id" = "t2"."trip_id_id") INNER JOIN "stops" 
                AS t3 ON ("t2"."stop_id_id" = "t3"."stop_id") 
                    WHERE ((("t1"."route_id_id" = '51') 
                            AND ("t3"."stop_code" = '51176')) 
                            AND ("t1"."service_id" 
                                 = (SELECT "t4"."service_id" 
                                    FROM "calendar_dates" AS t4 
                                    WHERE ("t4"."date" = '20180707')))) 
                    ORDER BY "t2"."departure_time";

sqlite> select * from calendar_dates where date = "20180706";
117|18U_SV|20180706|1
118|18U_S|20180706|1
















        """
        
        SELECT "t1"."calendar_id", "t1"."service_id", "t1"."date", "t1"."exception_type" 
        FROM "calendar_dates" 
        AS t1 
        WHERE("t1"."date"='20180711') 
        LIMIT 1 
        OFFSET 0'
        
        
        
        
        SELECT "t1"."service_id" 
        FROM "calendar_dates" 
        AS t1 
        WHERE("t1"."date"='20180711')'
        
        
        SELECT "t2"."departure_time" 
        FROM "trips" AS t1 
        INNER JOIN "stop_times" AS t2 
        ON("t1"."trip_id"="t2"."trip_id_id") 
        INNER JOIN "stops" AS t3 ON("t2"."stop_id_id"="t3"."stop_id") 
        WHERE((("t1"."route_id_id"='51') AND("t3"."stop_code"='51176')) AND True) 
        ORDER BY "t2"."departure_time";


SELECT "t2"."departure_time" 
FROM "trips" AS t1 
INNER JOIN "stop_times" AS t2 
ON ("t1"."trip_id" = "t2"."trip_id_id") 
INNER JOIN "stops" AS t3 
ON ("t2"."stop_id_id" = "t3"."stop_id") 
WHERE ((("t1"."route_id_id" = ?) 
        AND ("t3"."stop_code" = ?)) 
        AND ("t1"."service_id" .in_
             (SELECT "t4"."service_id" 
              FROM "calendar_dates" 
              AS t4 WHERE ("t4"."date" = ?)))) 
ORDER BY "t2"."departure_time"', ['51', '51176', '20180711'])




        retour = stm.get_prochain_horaires(date_time)
        print(retour)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_get_horaire']
    unittest.main()
