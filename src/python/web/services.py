# -*- coding: latin-1 -*-
'''
@author: jingl3s

Fichiers contenant les liens de la page web
'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import os

from bottle import get, post, response, request
from bottle import static_file


g_dossier_fichier = None
g_dossier_static_resource = None
interact = None

fct_return_service = None

# ######### BUILT-IN ROUTERS ###############


@get('/__exit')
@post('/__exit')
def __exit():
    global server
    server.stop()


@get('/web/<filepath:path>')
def server_static(filepath):
    global g_dossier_static_resource
    return static_file(filepath, root=g_dossier_static_resource)


@get('/<filepath:path>')
def server_static2(filepath):
    global g_dossier_fichier
    return static_file(filepath, root=g_dossier_fichier)

######### WEBAPP ROUTERS ###############


@get('/')
@post('/')
def home():
    global interact
    # Allo any origin https://github.com/bottlepy/bottle/issues/949
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Content-type'] = 'text/html'
    # print request.body.readlines()
#     print( request.headers.environ)
    return interact.obtenir_contenu().encode("utf-8")


def home_rien():
    return {"Success": True}


######### WEBAPP ROUTERS ###############


def set_interraction(p_interact):
    global interact
    interact = p_interact


def set_configuration(configuration):
    global fct_return_service
    fct_return_service = home

    global g_dossier_fichier
    g_dossier_fichier = configuration['dossier']

    global g_dossier_static_resource
    g_dossier_static_resource = os.path.join(os.path.abspath(
        os.path.dirname(__file__)),
        "..", "..",
        "resources",
        configuration['rsrc_folder'])
    pass
