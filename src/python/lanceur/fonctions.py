# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging


def calcul_temps_attente(p_dt_heure_courante, p_s_heure_calcul="21:13", p_i_delta_attente=0):
    try:
        # Conversion vers format datetime
        dt_heure_dernier_bus = datetime.datetime.strptime(
            p_s_heure_calcul,
            "%H:%M")

        # Calcul difference en secondes
        minute_pour_dernier_bus = (
            dt_heure_dernier_bus - p_dt_heure_courante).seconds / 60

        # Calcul specifique pour changement de jour
        if minute_pour_dernier_bus > (24 * 60 * 60):
            minute_pour_dernier_bus = (
                dt_heure_dernier_bus - datetime.datetime.strptime(
                    "00:00",
                    "%H:%M")).seconds / 60

        # Ajout d'un delta pour etre sur d'etre dans les temps
        attente = minute_pour_dernier_bus - p_i_delta_attente

    except ValueError as e:
        _logger = logging.getLogger(__name__)
        _logger.error(e)
        attente = 1000
    return int(attente)


if __name__ == '__main__':
    heure_courante = datetime.datetime.now()
    dernier_bus = "21:13"
    attente = calcul_temps_attente(
        heure_courante, p_s_heure_calcul=dernier_bus)
    print("Attente en secondes: {0}".format(attente))

    heure_courante = dt_heure_dernier_bus = datetime.datetime.strptime(
        "23:45",
        "%H:%M")
    dernier_bus = "00:15"
    attente = calcul_temps_attente(
        heure_courante, p_s_heure_calcul=dernier_bus)
    print("Attente en secondes: {0}".format(attente))
