#-*-coding:utf8;-*-
'''
@author: jingl3s
'''

# license
#
# Producer 2017 jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import os
import time
import logging
import logging.handlers


class LoggerConfig(object):
    '''
    classdocs
    '''
    TEMP_RAM_DIR="/mnt/tmpfs"

    def __init__(self, output_dir, file_basename, logger_level="DEBUG"):
        '''
        Constructor
        '''
        self.__logger = None
        if logger_level is "INFO":
            logger_level_select = logging.INFO
        elif logger_level is "ERROR":
            logger_level_select = logging.ERROR
        elif logger_level is "WARNING":
            logger_level_select = logging.WARNING
        else:
            logger_level_select = logging.DEBUG
        
        if file_basename is None:
            file_basename = "logging_file"
        self.__initialize_logger(output_dir, file_basename, logger_level_select)

    def __initialize_logger(self, output_dir, file_basename, logger_level_select):
        '''
        Method permettant de definir le logger
        '''
        self.__logger = logging.getLogger()
        self.__logger.setLevel(logger_level_select)
        formatter = logging.Formatter(
            "%(asctime)s-%(levelname)7s-%(module)s-%(funcName)s-%(message)s")

        # create console handler and set level to info
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        self.__logger.addHandler(handler)

        # create error file handler and set level to error
#        if not os.path.exists(output_dir):
#            os.mkdir(output_dir)
#         handler = logging.FileHandler(os.path.join(output_dir, file_basename + "_" +  time.strftime("%Y.%m.%d_%H.%M.%S") + ".log"),"w", encoding=None, delay="true")
#         handler.setFormatter(formatter)
#         self.__logger.addHandler(handler)

        # create error file handler rotating and set level to error
        output_dir = self.TEMP_RAM_DIR
        if os.path.exists(self.TEMP_RAM_DIR):
            log_file = os.path.join(self.TEMP_RAM_DIR, file_basename + ".log")
#
            handler = logging.handlers.RotatingFileHandler(log_file, "w", maxBytes=2097152,
                                                           backupCount=2)
            handler.setFormatter(formatter)
            self.__logger.addHandler(handler)
            
#         else:
#             log_file = file_basename + ".log"

    def get_logger(self):
        '''
        Retourne le logger actuel
        '''
        return self.__logger
