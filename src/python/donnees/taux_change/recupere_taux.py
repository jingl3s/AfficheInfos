# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import json
import logging

from tools.request_url import RequestURL


class RecupereTaux(object):
    '''
    Retrieve change rate
    '''

    def __init__(self):
        '''

        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._derniere_lecture = list()
        self._datetime_prochaine_mise_a_jour = datetime.datetime.now()
        self._alpha_key = ""
        self._indice_alpha = 0
        self._indice_fixer = 1
        self._request_url_alpha = RequestURL()

        self._derniere_lecture.append(
            {"taux": "NON-DISPO", "titre": "alphavantage", "taux_f": 0, "indication": 0})
#         self._derniere_lecture.append(
#             {"taux": "NON-DISPO", "titre": "fixer", "taux_f": 0, "indication": 0})

    def set_alpha_key(self, p_key):
        self._alpha_key = p_key
        self._request_url_alpha.set_url("https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=CAD&to_currency=EUR&apikey=" + self._alpha_key)

    def obtenir_taux(self, p_datetime):
        dt_courant = datetime.datetime.now()
        mise_a_jour = self._datetime_prochaine_mise_a_jour < dt_courant

        if mise_a_jour:
            capteur_trouve = False
            try:
                json_object = json.loads(self._url_request_alphavantage())
                # dict: {'Realtime Currency Exchange Rate': {'4. To_Currency
                # Name': 'Euro', '3. To_Currency Code': 'EUR', '1.
                # From_Currency Code': 'CAD', '6. Last Refreshed': '2018-01-21
                # 03:15:16', '7. Time Zone': 'UTC', '5. Exchange Rate':
                # '0.65483646', '2. From_Currency Name': 'Canadian Dollar'}}
                if 'Realtime Currency Exchange Rate' in json_object:
                    if "5. Exchange Rate" in json_object['Realtime Currency Exchange Rate']:
                        taux_alpha = json_object['Realtime Currency Exchange Rate']["5. Exchange Rate"]

                        taux_str = "{0:0.5f}".format(float(taux_alpha))

                        if self._derniere_lecture[self._indice_alpha]["taux_f"] != 0 and self._derniere_lecture[self._indice_alpha]["taux_f"] < taux_alpha:
                            self._derniere_lecture[self._indice_alpha]["indication"] = -1
                        else:
                            self._derniere_lecture[self._indice_alpha]["indication"] = 0

                        self._derniere_lecture[self._indice_alpha]["taux"] = taux_str
                        self._derniere_lecture[self._indice_alpha]["taux_f"] = taux_alpha
                        capteur_trouve = True

            except Exception as e:
                self._logger.error(
                    "Erreur lecture information alphavantage, execution continue {}".format(e))

#             capteur_trouve_fixer = False
#             try:
#                 json_object = json.loads(self._url_request_fixer())
#                 # {"base":"EUR","date":"2018-01-12","rates":{"CAD":1.5194}}
#                 if 'rates' in json_object:
#                     if "CAD" in json_object['rates']:
#                         taux_fixer = json_object['rates']["CAD"]
#
#                         taux_util = 1.0 / taux_fixer
#
#                         taux_str = "{0:0.5f}".format(taux_util)
#
#                         if self._derniere_lecture[self._indice_fixer]["taux_f"] != 0 and self._derniere_lecture[self._indice_fixer]["taux_f"] < taux_util:
#                             self._derniere_lecture[self._indice_fixer]["indication"] = -1
#                         else:
#                             self._derniere_lecture[self._indice_fixer]["indication"] = 0
#
#                         self._derniere_lecture[self._indice_fixer]["taux"] = taux_str
#                         self._derniere_lecture[self._indice_fixer]["taux_f"] = taux_util
#                         capteur_trouve_fixer = True
#
#             except Exception as e:
#                 self._logger.error(
#                     "Erreur lecture information fixer, execution continue" + str(e))

            if not capteur_trouve:
                self._logger.debug("alphavantage serveur non répondu.")
                self._datetime_prochaine_mise_a_jour += datetime.timedelta(
                    hours=1)
#             elif not capteur_trouve_fixer:
#                 self._logger.debug("fixer serveur non répondu.")
#                 self._datetime_prochaine_mise_a_jour += datetime.timedelta(
#                     hours=1)
            else:
                self._datetime_prochaine_mise_a_jour += datetime.timedelta(
                    days=1)

        return self._derniere_lecture

    def _url_request_alphavantage(self):
        read_value =  self._request_url_alpha.get_url_request()
        return read_value

#     def _url_request_fixer(self):
#         try:
#             #             response = requests.get(
#             #                 "https://api.fixer.io/latest?symbols=EUR,CAD", timeout=(3.0,15.0))
#             #             response = requests.get(
#             #                 "https://api.fixer.io/latest?symbols=EUR,CAD", timeout=3.0)
#             response = requests.get(
#                 "https://data.fixer.io/api/latest?symbols=EUR,CAD", timeout=3.0)
# 
#             return response.text
#         except Exception as e:
#             raise Exception(e)
#         return None


if __name__ == "__main__":
    taux = RecupereTaux()
    taux.set_alpha_key("TEST")
    taux_lus = taux.obtenir_taux(datetime.datetime.now())
    print(taux_lus)
