# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging
import os
import subprocess
import sys
import time
import json


class Android(object):
    '''
    Classe utilise pour intwrragir avec Android
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)


    def is_wifi(self):
        is_network_avail = None
        try:
            cmd_io = subprocess.check_output("termux-wifi-connectioninfo")
            if cmd_io is not None:
                json_cmd = json.loads(cmd_io)
                is_network_avail = json_cmd['supplicant_state'] != 'DISCONNECTED'
                if is_network_avail:
                    self._logger.debug("termux state {}".format(
                        json_cmd['supplicant_state']))
        except FileNotFoundError:
            self._logger.error("No termux api connection status available")
        return is_network_avail
        


    def set_wifi_state(self, new_state:bool):
        is_network_avail = None
        try:
            cmd_io = subprocess.check_call(["termux-wifi-enable", '{}'.format(new_state).lower()])
        except FileNotFoundError:
            self._logger.error("No termux api connection status available")
        return is_network_avail
        
    def screen_off(self):
        '''
        Sources
            - https://stackoverflow.com/questions/7585105/turn-on-screen-on-device
        Alternate method
        KEYCODE_POWER
        dumpsys display|grep mScreenState

        Commande termux
        env -i USER=shell "$(PATH=/system/xbin:/system/bin:/su/bin:/sbin:/magisk/.core/bin which su)" shell --context u:r:shell:s0 --shell /system/bin/sh --command input keyevent KEYCODE_SLEEP
        '''
        with subprocess.Popen(['su', '-c', 'input', 'keyevent', 'KEYCODE_SLEEP']) as proc:
            proc.communicate()

    def screen_on(self):
        '''
        Sources
            - https://stackoverflow.com/questions/7585105/turn-on-screen-on-device
        '''
        with subprocess.Popen(['su', '-c', 'input', 'keyevent', 'KEYCODE_WAKEUP']) as proc:
            proc.communicate()

    def lock(self):
        try:
            with subprocess.Popen('termux-wake-lock') as proc:
                proc.communicate()
        except:
            # Ignore error to permit continue process and cases debug on non
            # android
            pass


if __name__ == "__main__":
    andro = Android()
    andro.set_wifi_state(True)
    andro.set_wifi_state(False)
    
    andro.set_wifi_state(True)
