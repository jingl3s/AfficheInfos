import logging


class StmApiRetriever:
    def __init__(self):
        self._stm_api = None
        self._bus_stop = None
        self._list_bus_number = None
        self._logger = logging.getLogger(self.__class__.__name__)

    def set_stm_api(self, stm_api):
        self._stm_api = stm_api

    def set_json_config(self, p_config: dict):
        if "ARRET" in p_config:
            self._bus_stop = p_config["ARRET"]

        if "BUS" in p_config:
            self._list_bus_number = list(p_config["BUS"].keys())

    def get_prochain_horaires(self, dt_courant):
        stm_api_results = self._stm_api.get_bus_update()
        if self._bus_stop in stm_api_results:
            return stm_api_results[self._bus_stop]

