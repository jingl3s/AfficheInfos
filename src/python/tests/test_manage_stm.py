# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging
from requests import Session
import socket
from unittest import mock
import unittest
from unittest.mock import patch

from donnees.horaire_stm.manage_stm import ManageSTM
from donnees.horaire_stm.stm2 import Stm2


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass
    
    
    @patch("donnees.horaire_stm.stm_real_time.session")
    def testManageStm(self, mock_class_session):
        # Disable Session to have only time bus from local database
        session_instance = mock_class_session.return_value
        
        logging.basicConfig(level=logging.DEBUG)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(logging.DEBUG)

        # Objet : STM
        self._stm = ManageSTM()
        
        # Configure without real time usage
        self._stm.set_json_config({'TITRE': 'SNOWDON', 'NUMBER_DB_PER_BUS': 10, 'BUS': {'166': {'REAL_TIME': {}}, '51': {'REAL_TIME': {}}, '711': {'REAL_TIME': {}}}, 'ARRET': '51176'})
        
        hostname = socket.gethostname()
        if hostname in 'soniahugo-B85M-HD3':
            dossier_trouve = "/home/soniahugo/.stmcli"
        else:
            #            dossier_trouve = "/data/data/com.termux/files/home/
            dossier_trouve = "/data/data/com.termux/files/home/.stmcli"
            
        self._stm.set_stm_data_dir(dossier_trouve)
        self._logger.debug('db stm selected {}'.format(dossier_trouve))

        self._stm_g = Stm2()
        self._stm_g.set_stm_manager(self._stm)
        self._stm_g.set_temps_decalage(3)

        self._dt_heure_courante = datetime.datetime.now()

        sortie = self._stm_g.get_horaires(self._dt_heure_courante)
        self._logger.debug("Horaires trouvé v2: {}".format(sortie))

        sortie = self._stm_g.get_horaires(self._dt_heure_courante + datetime.timedelta(
            minutes=2))
        self._logger.debug("Horaires trouvé v2: {}".format(sortie))

        # FIXME: there is an error as on 0h the nexxt bus is only 5h
        sortie = self._stm_g.get_horaires(self._dt_heure_courante.replace(hour=0) + datetime.timedelta(
            minutes=2))
        self._logger.debug("Horaires trouvé v2: {}".format(sortie))

    def testManageStmRealTime(self):
        
        logging.basicConfig(level=logging.DEBUG)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(logging.DEBUG)

        # Objet : STM
        self._stm = ManageSTM()
        self._stm.set_json_config({'TITRE': 'SNOWDON', 'NUMBER_DB_PER_BUS': 10, 'BUS': {'166': {'REAL_TIME': {'NUMBER': 2, 'DIR': 'North'}}, '51': {'REAL_TIME': {'NUMBER': 4, 'DIR': 'West'}}, '711': {'REAL_TIME': {}}}, 'ARRET': '51176'})
        
        hostname = socket.gethostname()
        if hostname in 'soniahugo-B85M-HD3':
            dossier_trouve = "/home/soniahugo/.stmcli"
        else:
            #            dossier_trouve = "/data/data/com.termux/files/home/
            dossier_trouve = "/data/data/com.termux/files/home/.stmcli"
            
        self._stm.set_stm_data_dir(dossier_trouve)
        self._logger.debug('db stm selected {}'.format(dossier_trouve))

        self._stm_g = Stm2()
        self._stm_g.set_stm_manager(self._stm)
        self._stm_g.set_temps_decalage(3)

        self._dt_heure_courante = datetime.datetime.now()

        sortie = self._stm_g.get_horaires(self._dt_heure_courante)
        # .get_prochain_horaires2(self._dt_heure_courante)
        self._logger.debug("--- Horaires trouvé v2: {}".format(sortie))

        sortie = self._stm_g.get_horaires(self._dt_heure_courante + datetime.timedelta(
            minutes=2))
        self._logger.debug("--- Horaires trouvé v2: {}".format(sortie))
        
        
        
if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testManageStm']
    unittest.main()
