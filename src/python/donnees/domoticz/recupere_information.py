'''
@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import datetime
from enum import Enum
import json
import logging

from tools.request_url import RequestURL


class Capteur(Enum):
    EXT = 1
    INT = 2
    ELEC = 4
    I_WIFI = 3
    E_RES = 5
    E_EOL = 6
    E_HUM = 7


class Domoticz(object):
    '''
    Classe utilisé pour récupérer des information du serveur domoticz
    '''
    DOMOTICZ_SERVER_ADDRESS = "192.168.254.194:8080"
    DOMOTICZHTTP = "http://"
    DOMOTICZURLCAPT = "/json.htm?type=devices&rid="

    def __init__(self):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._url_domoticz = None

        self._temperatures_lues = dict()

        self._association_capteurs = dict()
        for capteur in Capteur:
            self._association_capteurs[capteur] = None
            self._datetime_en_cours = datetime.datetime.now()
        self._request_url = RequestURL()
        self._request_url.set_enable_retries(False)

    def set_domoticz_server_address(self, server_address):
        self._url_domoticz = self.DOMOTICZHTTP + server_address + self.DOMOTICZURLCAPT

    def set_id_capteur(self, capteur, p_id):
        self._association_capteurs[capteur] = p_id

    def get_temperature(self, capteur):
        if capteur in Capteur:
            id_capteur = self._association_capteurs[capteur]
            if id_capteur is None:
                raise RuntimeError(
                    "ID Capteur non défini pour capteur {0}.".format(capteur))
            elif not capteur.name in self._temperatures_lues:
                self._temperatures_lues[capteur.name] = [None, None]
        else:
            raise RuntimeError("ID Capteur invalide.")

        if capteur is Capteur.E_RES:
            temperature, dt_derniere_lecture = self._domoticz_temperature(
                id_capteur, "DewPoint")
        else:
            temperature, dt_derniere_lecture = self._domoticz_temperature(
                id_capteur)

        if temperature is not None or dt_derniere_lecture is not None:
            self._temperatures_lues[capteur.name] = [
                temperature, dt_derniere_lecture]

        return self._temperatures_lues[capteur.name]

    def get_toutes_temperatures(self, p_datetime):
        self._datetime_en_cours = p_datetime
        for capteur in Capteur:
            if self._association_capteurs[capteur] is not None:
                return_vals = self.get_temperature(capteur)
                # Arret boucle si une erreur detectée
                if None is return_vals or None in return_vals:
                    break
        return self._temperatures_lues

    def _domoticz_temperature(self, id_capteur, json_domoticz_lecture="Temp"):
        date_last_update = None
        temperature = None
        dt_derniere_observation = None
        capteur_trouve = False
        try:
            url_out = self._url_request(id_capteur)
            if url_out is not None:
                json_object = json.loads(url_out)
            else:
                json_object = None
#             self._logger.debug(json_object)

            if json_object is not None and "status" in json_object:
                temperature = 0
                if json_object["status"] == "OK":
                    try:
                        for i, _ in enumerate(json_object["result"]):
                            if json_object["result"][i]["idx"] == str(id_capteur):
                                prend_en_compte_val = False
                                if 'LastUpdate' in json_object["result"][i]:
                                    last_update = json_object["result"][i]['LastUpdate']
                                    date_last_update = datetime.datetime.strptime(
                                        last_update, '%Y-%m-%d %H:%M:%S')
                                    last_diff = datetime.datetime.now() - date_last_update
                                    if last_diff.seconds <= 60 * 60 or 'HydroQuebec' == json_object["result"][i]['Name']:
                                        prend_en_compte_val = True

                                    # FIXME: Hardfix to avoid issue with wifi
                                    # sensor
                                    if id_capteur == 43:
                                        prend_en_compte_val = True
                                else:
                                    prend_en_compte_val = True
                                if prend_en_compte_val:
                                    capteur_trouve = True
                                    temperature = json_object["result"][0][json_domoticz_lecture]
                                else:
                                    temperature = 'N/A'
                    except Exception as e:
                        try:
                            temperature = json_object["result"][0]["Level"]
                        except Exception as e:
                            try:
                                temperature = json_object["result"][0]["CounterToday"]
                            except Exception as e:
                                self._logger.error(
                                    "Erreur lecture information domoticz, execution continue {}".format(e))
        except Exception as e:
            self._logger.error(
                "Erreur lecture information domoticz, execution continue {}".format(e))

        if date_last_update is not None:
            dt_derniere_observation = date_last_update
        else:
            dt_derniere_observation = datetime.datetime.now()

        if not capteur_trouve:
            self._logger.debug("Domoticz serveur ou Capteur non répondus.")

        return temperature, dt_derniere_observation

    def _url_request(self, id_capteur):
        self._request_url.set_url(self._url_domoticz + str(id_capteur))
        return self._request_url.get_url_request()


if __name__ == "__main__":
    domoticz_id_ext = 27
    domoticz_id_int = 10
    domoticz_id_wifi = 43
    domoticz = Domoticz()
    domoticz.set_domoticz_server_address(Domoticz.DOMOTICZ_SERVER_ADDRESS)
    domoticz.set_id_capteur(Capteur.EXT, domoticz_id_ext)
    domoticz.set_id_capteur(Capteur.INT, domoticz_id_int)
    domoticz.set_id_capteur(Capteur.I_WIFI, domoticz_id_wifi)
    domoticz.set_id_capteur(Capteur.E_RES, domoticz_id_ext)
    domoticz.set_id_capteur(Capteur.ELEC, 55)
    domoticz.set_id_capteur(Capteur.E_EOL, 62)

    temperature = domoticz.get_temperature(Capteur.EXT)
    print("Temperature: {0}:{1}".format(Capteur.EXT.name, temperature))

    temperature = domoticz.get_temperature(Capteur.INT)
    print("Temperature: {0}:{1}".format(Capteur.INT.name, temperature))

    temperature = domoticz.get_temperature(Capteur.I_WIFI)
    print("Temperature: {0}:{1}".format(Capteur.I_WIFI.name, temperature))

    temperature = domoticz.get_toutes_temperatures(datetime.datetime.now())
    print("Temperature: {0}".format(temperature))
