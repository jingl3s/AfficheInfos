# -*- coding: latin-1 -*-
"""
@author: jingl3s

"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import copy
import logging
import os

from stmcli import bus, data, database

from donnees.horaire_stm.manage_stm_rt import ManageSTMRealTime
from donnees.horaire_stm.info_bus import InformationBus


class ManageSTM(object):
    """
    Gestion des horaires STM en utilisant stmcli et un module pour recuperer le temps reel 

    """

    def __init__(self):
        """
        Constructor
        """
        self._logger = logging.getLogger(self.__class__.__name__)
        self._bus_number = None
        self._bus_stop = None
        self._number_departure = 3
        self._stmcli_data_dir = "{0}/.stmcli/".format(os.path.expanduser("~"))
        self._db_file = "{0}/stm.db".format(self._stmcli_data_dir)
        self._bus_rt_time = dict()
        self._titre = ""
        self._list_next_horaire2 = list()
        self._stm_rt = ManageSTMRealTime()
        self._temps_decalage_acces_arret = 0
        self._is_update_waiting = False
        self._update_failed = False
        self._last_update_failed = False
        self._stm_api_extra = None

        # Disable PEEWEE logging by patching the logger
        logger = logging.getLogger("peewee")
        logger.setLevel(logging.INFO)

    def set_nombre_horaire(self, nombre_horaires):
        self._number_departure = nombre_horaires

    def set_temps_decalage(self, temps):
        self._temps_decalage_acces_arret = temps

    def set_stm_data_dir(self, p_s_path):
        """
        :param p_s_path:
        :return True if database file is found in the given directory 
        """
        self._stmcli_data_dir = p_s_path
        self._db_file = "{0}/stm.db".format(self._stmcli_data_dir)
        is_valid_db_file = os.path.exists(self._db_file)
        if is_valid_db_file:
            self._logger.debug("Stm db {}".format(self._db_file))
        return is_valid_db_file

    def set_bus_stop(self, bus_stop):
        """

        :param bus_stop: string of bus stop number
        """
        self._bus_stop = bus_stop

    def set_list_bus_number(self, list_bus_number):
        """

        :param list_bus_number: list of bus number as string
        """
        self._bus_number = list_bus_number

    def set_json_config(self, p_config: dict):
        if "ARRET" in p_config:
            self.set_bus_stop(p_config["ARRET"])

        if "BUS" in p_config:
            self.set_list_bus_number(list(p_config["BUS"].keys()))

        if "TITRE" in p_config:
            self._titre = p_config["TITRE"]

        if "NUMBER_DB_PER_BUS" in p_config:
            self.set_nombre_horaire(p_config["NUMBER_DB_PER_BUS"])

        self._stm_rt.set_json_config(p_config)

    def set_stm_api_extra(self, stm_api_extra):
        self._stm_api_extra = stm_api_extra

    def get_prochain_horaires(self, dt_courant):
        """
        Retour du nombre d'horaire de bus
        Equivalent appel successif 
        stmcli -s 51176 -b 51 -n 3

        @return: list des horaires de bus ordonnee sans information du bus
        """
        self._logger.debug("Recherche des horaires")

        #         import sys;sys.path.append(r'/opt/eclipse/plugins/org.python.pydev_6.0.0.201709191431/pysrc')
        #         import pydevd;pydevd.settrace('192.168.254.199')

        # Verification si informations definies
        if self._verification_entree():
            try:
                self._obtenir_horaires(dt_courant)
            except:
                self._logger.exception("Erreur STM")

        self._list_next_horaire2.sort()

        return {"TITRE": self._titre, "HORAIRE": self._list_next_horaire2}

    def _obtenir_horaires(self, dt_courant):

        self._mise_a_jour_horaire()

        self._get_horaires(dt_courant)
        self._bus_rt_time = self._stm_rt.get_prochain_horaires(dt_courant)
        if self._stm_api_extra is not None:
            self._bus_api_time = self._stm_api_extra.get_prochain_horaires(dt_courant)

        # Si le resultat est nul, il faut peut etre faire une mise a jour
        if (
            len(self._list_next_horaire2) == 0
            and self._is_update_waiting
            and not self._update_failed
            and not self._last_update_failed
        ):
            info_bus_err = InformationBus("Update waiting", "ATTENTION", p_is_error_msg=True)
            self._list_next_horaire2.append(info_bus_err)
            self._logger.warning("DB update waiting")
            self._create_time_from_other_source(dt_courant)

        elif len(self._list_next_horaire2) == 0 or self._update_failed:
            self._logger.error("Aucun nouvel horaire trouve")
            info_bus_err = InformationBus("Aucun horaire trouve !", "ERREUR", p_is_error_msg=True)
            self._list_next_horaire2.append(info_bus_err)
            self._last_update_failed = True
            self._create_time_from_other_source(dt_courant)
        else:

            self._update_bus_real_time2()

            if self._stm_api_extra is not None:
                self._update_bus_api_time2(dt_courant)

            # As valid schedule found restore waiting to not expected
            self._is_update_waiting = False
            
    def _create_time_from_other_source(self, dt_courant):
        '''
        Ajout des horaire temps reel dans la sortie dans le cas ou impossible d'en avoir autrement
        :param dt_courant:
        '''
        
        # Add real time bus schedule
        for bus_id, bus_value in self._bus_rt_time.items():
            for bus_time_left in bus_value:
                info_bus = InformationBus(bus_id, "-", bus_time_left, temps_attente_reel=bus_time_left, p_dt_heure_courante=dt_courant)
                info_bus.update_temps_attente(dt_courant)
                self._list_next_horaire2.append(info_bus)
                
        if self._stm_api_extra is not None:
            for bus_id, bus_value in self._bus_api_time["ARRIVALS_FULL"].items():
                for cur_bus_data in bus_value:
                    next_real_time_departure = cur_bus_data["timef"]
                    next_real_time_departure_compute = int((next_real_time_departure - dt_courant).seconds / 60)
                    info_bus = InformationBus(bus_id, "-", p_dt_heure_courante=dt_courant)
                    info_bus.add_extra_time("api", next_real_time_departure, next_real_time_departure_compute)
                    info_bus.add_extra_data("api_full", cur_bus_data)
                    self._list_next_horaire2.append(info_bus)
                    
        # Nettoyage liste pour la generation
        self._list_next_horaire2.sort()
        self._list_next_horaire2 = self._list_next_horaire2[: self._number_departure]
        
    def _update_bus_real_time2(self):
        """
        Retrieve bus update from real time
        """

        # Associate real time with waiting bus
        bus_rt_copy = copy.deepcopy(self._bus_rt_time)

        for bus_info in self._list_next_horaire2:
            bus_id = bus_info.bus_id

            # Add real time bus schedule
            bus_remaining_updated = False
            while bus_id in self._bus_rt_time and len(bus_rt_copy[bus_id]) > 0 and not bus_remaining_updated:
                next_real_time_departure = bus_rt_copy[bus_id][0]
                del bus_rt_copy[bus_id][0]

                bus_info.temps_attente_reel = next_real_time_departure
                bus_remaining_updated = True

    def _update_bus_api_time2(self, dt_courant):
        """
        Retrieve bus update from real time
        """

        # Associate real time with waiting bus
        bus_rt_copy = copy.deepcopy(self._bus_api_time["ARRIVALS_FULL"])

        list_time = dict()

        bus_api_data = self._bus_api_time["ARRIVALS_FULL"]

        for key in bus_api_data.keys():
            for value in bus_api_data[key]:
                if key not in list_time:
                    # if not isinstance(list_time[key], list):
                    list_time[key] = list()

                list_time[key].append(value["time"])

        for values in list_time.values():
            values.sort()

        for bus_info in self._list_next_horaire2:
            bus_id = bus_info.bus_id

            # Add real time bus schedule
            bus_remaining_updated = False

            while (
                bus_id in bus_api_data
                and bus_id in list_time
                and len(list_time[bus_id]) > 0
                and not bus_remaining_updated
            ):
                cur_time = list_time[bus_id][0]
                for item in bus_api_data[bus_id]:
                    if cur_time == item["time"]:
                        del list_time[bus_id][0]
                        cur_bus_data = item
                        break

                next_real_time_departure = cur_bus_data["timef"]
                next_real_time_departure_compute = int((next_real_time_departure - dt_courant).seconds / 60)

                bus_info.add_extra_time("api", next_real_time_departure, next_real_time_departure_compute)
                bus_info.add_extra_data("api_full", cur_bus_data)
                bus_remaining_updated = True

    def _mise_a_jour_horaire(self):

        if not os.path.isdir(self._stmcli_data_dir):
            os.makedirs(self._stmcli_data_dir)

        # Checking for updates
        self._logger.debug("Mise a jour de la base des horaires si aucun horaire disponible.")
        try:

            # Internal state to delay update and ensure no infinite tries
            if self._is_update_waiting and not self._update_failed and not self._last_update_failed:
                self._logger.error("DB update in progress")
                data.check_for_update(self._db_file, self._stmcli_data_dir, force_update=True)

                if not os.path.exists(self._db_file):
                    self._logger.error("Mise a jour de la base a echoue.")
                    self._update_failed = True
                    self._last_update_failed = True
                else:
                    self._is_update_waiting = False
            else:
                self._is_update_waiting = True

        except Exception:
            self._update_failed = True
            self._last_update_failed = True
            self._logger.error("Update DB error", exc_info=1)

    def _get_horaires(self, dt_courant):

        date = dt_courant.strftime("%Y%m%d")
        custom_time = dt_courant.strftime("%H:%M").split(":")

        next_departures = list()

        # Assure que la base de donn�e est configur�
        database.init_database(self._db_file)

        del self._list_next_horaire2[:]

        # Recherche des horaire pour chaque bus demand�
        for bus_id in self._bus_number:
            try:
                next_departures = bus.next_departures(
                    bus_id, self._bus_stop, date, custom_time, self._number_departure, self._db_file
                )

                for horaire in next_departures:
                    heure_formate = self._format_heure(horaire)
                    info_bus = InformationBus(bus_id, heure_formate)
                    info_bus.update_temps_attente(dt_courant)
                    self._list_next_horaire2.append(info_bus)

            except:
                self._logger.error("Unable to get bus schedule for {}".format(bus_id))
                break

        if 0 < len(self._list_next_horaire2):
            # Sort and reduce size in order to have only the expected values
            self._list_next_horaire2.sort()
            self._list_next_horaire2 = self._list_next_horaire2[: self._number_departure]

    def _format_heure(self, horaire):
        """
        Formate l'heure sur un format correct car STM retourne des heures invalides apres 23:59
        Il affiche par exemple 24:15, 25:42
        :param horaire: chaine de caractere avec s�parateur repr�sentant un horaire HH:MM
        """
        dep_time = horaire.split(":")
        hour = int(dep_time[0])
        if hour > 23:
            hour_new = str(hour - 24)
        else:
            hour_new = str(hour)

        horaire__formate = hour_new + ":" + dep_time[1]
        return horaire__formate

    def _verification_entree(self):
        sortie = self._bus_stop is not None
        sortie &= self._bus_number is not None

        return sortie
