# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import json
import logging
import os
import requests
import sys
import time

import schedule

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from common.logger_config import LoggerConfig
from tools.request_url import RequestURL


class BackgroundUpdate(object):
    '''
    Classe utilise pour mettre a jpur le serveur d'affichage en tache de fond ppur syncrhoniser avec les disppnibiliyes du wifi
    '''
    DOMOTICZ_SERVER_ADDRESS = "192.168.254.194:8080"
    DOMOTICZHTTP = "http://"
    DOMOTICZURLCAPT = "/json.htm?type=devices&rid="
    DOMOTICZURLCAPT_MODIF = "/json.htm?type=command&param=switchlight&idx=__IDX__&switchcmd="
    DOMOTICZURL_SCHEDUL = "/json.htm?type=schedules&filter=device"
    DOMOTICZ_ID_WIFI = 43

    DEFAULT_TIME_BETWEEN_UPDATE = 2 * 60
    DEFAULT_TIME_BETWEEN_UPDATE = 10

    def __init__(self):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._url_domoticz = None
        self._server_address = None
        self._time_sleep_between_update = self.DEFAULT_TIME_BETWEEN_UPDATE
        self._request_url = RequestURL()
        self._next_update_schedule = datetime.datetime.now()
        self._interface = None
        self._command = None

    def set_domoticz_server_address(self, server_address):
        self._url_domoticz = self.DOMOTICZHTTP + server_address + self.DOMOTICZURLCAPT
        self._url_domoticz_ecrit = self.DOMOTICZHTTP + \
            server_address + self.DOMOTICZURLCAPT_MODIF
        self._server_address = server_address

    def set_time_sleep_between_update(self, p_delay_seconds: float):

        self._time_sleep_between_update = p_delay_seconds

    def set_interface(self, interface):
        self._interface = interface

    def set_command(self, cmd):
        self._command = cmd

    def run_loop(self):
        """
        Main loop of program with sceduling hours defined as hardcoded
        Sleep the program until next task to run
        """
        self._interface.lock()
        self._scheduler_rules()
        self._scheduler_rules_screen()
        self.run_background()
        while True:
            self._logger.debug("Init processing")
            schedule.run_pending()
            sleep_time_fct = schedule.idle_seconds
            self._logger.debug("Next run: {}".format(schedule.next_run()))
            self._wait(sleep_time_fct)

    def run_background(self):
        """
        Update processing by tryuing first one if network is available
        Try later based pn weather data code update time(source code)
        disable wifi at end as the wifi was spawned up here
        """
        self._logger.debug("enter background update")

        self._interface.screen_on()
        if self._update_server():
            time.sleep(self.DEFAULT_TIME_BETWEEN_UPDATE)
            self._interface.screen_on()
            self._command.exec_command()
            self._disable_wifi()
            self._update_scheduler_rules()
            self._interface.lock()

    def _update_server(self):
        update_requested = False

        counter = 0
        network_avail = False
        while not network_avail and counter < 10:
            time.sleep(30)
            network_avail = self._is_network_avail()
            counter += 1

        if network_avail:
            self._logger.debug("Enter server update")
            self._command.exec_command()
            update_requested = True
            self._last_update = datetime.datetime.now()
        return update_requested

    def _update_scheduler_rules(self):
        if self._next_update_schedule <= datetime.datetime.now():
            self._scheduler_rules()
            self._next_update_schedule = datetime.datetime.now() + datetime.timedelta(
                days=3)

    def _scheduler_rules(self):
        domoticz_shedules = self._domoticz_schedule(self.DOMOTICZ_ID_WIFI)

        fct_used = self.run_background

        if len(domoticz_shedules) > 0:
            schedule.clear('run-background')
            schedule.clear('screen-off')

            for domoticz_shedule in domoticz_shedules:
                if domoticz_shedule["state"]:
                    self._logger.debug("Add run background {}".format(domoticz_shedule["time"]))
                    schedule.every().day.at(domoticz_shedule["time"]).do(
                        fct_used).tag('run-background')
                else:
                    self._logger.debug("Add screen off {}".format(domoticz_shedule["time"]))
                    schedule.every().day.at(domoticz_shedule["time"]).do(
                        self._interface.screen_off).tag('screen-off')

            # Add more try on every pair hour during day
            for time_hour in range(8, 21, 2):
                
                time_start = "{:02}:00".format(time_hour)
                time_end = "{:02}:15".format(time_hour)
                
                self._logger.debug("Add run background {}".format(time_start))
                schedule.every().day.at(time_start).do(
                    fct_used).tag('run-background')
                    
                self._logger.debug("Add screen off {}".format(time_end))
                schedule.every().day.at(time_end).do(
                    self._interface.screen_off).tag('screen-off')
        #DEbug time every minutes
#         schedule.every(1).minutes.do(fct_used).tag('run-background')

    def _scheduler_rules_screen(self):

        schedule.every().day.at("06:30").do(
            self._interface.screen_on)#.tag("manual-screen-on")
        schedule.every().day.at("07:15").do(
            self._interface.screen_on).tag("manual-screen-on")
        schedule.every().day.at("07:50").do(
            self._interface.screen_off).tag("manual-screen-off")

    def _wait(self, p_delay_fct):
        p_delay = p_delay_fct()
        self._logger.debug("time sleep: {}".format(p_delay))
        if p_delay > 0:
            time.sleep(p_delay)
        self._logger.debug("end wait")
        return
        while p_delay > 0:
            self._logger.debug("Loop update last {}".format(p_delay))
            time.sleep(self.DEFAULT_TIME_BETWEEN_UPDATE)
            p_delay = p_delay_fct()
            if p_delay < self.DEFAULT_TIME_BETWEEN_UPDATE and p_delay > 0:
                time.sleep(p_delay)
            self._logger.debug("time sleep: {}".format(p_delay))

    def _is_network_avail(self) -> bool:
        """
        :return True if network is available
        """
        termux_state = self._interface.is_wifi()
        if termux_state is None:
            self._logger.error(
                "No Termux status detected, assume it is available")
            state = True
        else:
            state = termux_state
        return state

    def _disable_wifi(self):
        """
        verify if autonatic wifi activated and disable it
        """
        id_capteur = 68

        sensor_val = self._domoticz_sensor(
            id_capteur, json_domoticz_lecture="Status")

        if sensor_val is not None and sensor_val:
            id_capteur = 68
            self._domoticz_requete_eteindre(id_capteur)
            sensor_val = self._domoticz_sensor(
                id_capteur, json_domoticz_lecture="Status")

            if sensor_val is not None and sensor_val:
                self._logger.error("Switch change state error")

    def _domoticz_sensor(self, id_capteur, json_domoticz_lecture="Temp"):
        temperature = None
        try:
            self._request_url.set_url(self._url_domoticz + str(id_capteur))
            url_data = self._request_url.get_url_request()
            json_object = json.loads(url_data)

            if json_object is not None and "status" in json_object:
                temperature = 0
                if json_object["status"] == "OK":
                    try:
                        for i, _ in enumerate(json_object["result"]):
                            if json_object["result"][i]["idx"] == str(id_capteur):
                                json_lect = json_object["result"][0][json_domoticz_lecture]
                                if not json_lect.isdigit():
                                    if json_lect == "On":
                                        temperature = True
                                    elif json_lect == "Off":
                                        temperature = False
                            else:
                                temperature = 'N/A'
                    except Exception as e:
                        try:
                            temperature = json_object["result"][0]["Level"]
                        except Exception as e:
                            try:
                                temperature = json_object["result"][0]["CounterToday"]
                            except Exception as e:
                                self._logger.error(
                                    "Erreur lecture information domoticz, execution continue {}".format(e))
        except Exception as e:
            self._logger.error(
                "Erreur lecture information domoticz, execution continue")
        return temperature

    def _domoticz_schedule(self, id_capteur: int):
        schedul_avail = list()

        try:
            self._request_url.set_url(
                self.DOMOTICZHTTP + self._server_address + self.DOMOTICZURL_SCHEDUL)
            url_data = self._request_url.get_url_request()
            json_object = json.loads(url_data)

            if json_object is not None and "status" in json_object:
                if json_object["status"] == "OK":
                    try:
                        for i, _ in enumerate(json_object["result"]):
                            if id_capteur == json_object["result"][i]["DeviceRowID"]:
                                schedul_avail.append(
                                    {"time": json_object["result"][i]["Time"], "state": json_object["result"][i]["TimerCmd"] == 0})
                    except Exception as e:
                        self._logger.error(
                            "Erreur lecture information domoticz, execution continue {}".format(e))

        except Exception as e:
            self._logger.error(
                "Erreur lecture information domoticz, execution continue")
        return schedul_avail

    def _domoticz_requete_eteindre(self, id_capteur):
        '''
        @return: json structure de la reponse si OK pour le status
                    Sinon retourne None
        '''
        url_domoticz_ecrit = self._url_domoticz_ecrit.replace(
            "__IDX__", str(id_capteur))
        url_domoticz_ecrit = url_domoticz_ecrit + "Off"

        json_object = None
        try:
            response = requests.get(url_domoticz_ecrit)
            json_object = json.loads(response.text)
            if json_object["status"] != "OK":
                self._logger.error("Cmd URL a echoue. cmd : '{}', reponse : '{}'".format(
                    json_object, self._url_cmd_domoticz))
                json_object = None
        except Exception as e:
            raise Exception(e)
        return json_object


class Command():
    DOMOTICZHTTP = "http://"
    LOCAL_SERVER_ADDRESS = DOMOTICZHTTP + "0.0.0.0:8081"
    TIME_WAIT_BEFORE_STOP_MINUTE = 5 

    def __init__(self):
        self._command = None
        self._logger = logging.getLogger(self.__class__.__name__)

    def set_mode(self, mode):
        self._command = mode

    def exec_command(self):
        if self._command:
            self._update_local_server()
        else:
            self._wait_and_disable_wifi()

    def _update_local_server(self):
        self._logger.debug("init server update")
        try:
            # Big timeout as the update take more than few seconds to answer
            response = requests.get(self.LOCAL_SERVER_ADDRESS, timeout=25)

            if response.status_code != 200:
                self._logger.error("update error")
            response.close()
            self._logger.debug(response)
        except:
            self._logger.error("failed to communicate with local server")

    def _wait_and_disable_wifi(self):
        wait_time = self.TIME_WAIT_BEFORE_STOP_MINUTE * 60
        self._logger.debug("Wait some time: {}".format(wait_time))
        time.sleep(wait_time)


if __name__ == "__main__":
#     LoggerConfig(None, None, logger_level="DEBUG")
    LoggerConfig(None, None, logger_level="WARNING")

    cmd = Command()

    bu = BackgroundUpdate()
    bu.set_command(cmd)
    try:
        import orange_pi_wifi
        opi = orange_pi_wifi.OrangePi()
        bu.set_interface(opi)
        cmd.set_mode(False)
    except ImportError:
        import android
        andro = android.Android()
        bu.set_interface(andro)
        cmd.set_mode(True)

    #  bu.set_time_sleep_between_update(1.0)
    bu.set_domoticz_server_address(BackgroundUpdate.DOMOTICZ_SERVER_ADDRESS)
#     bu.run_background()
    bu.run_loop()
