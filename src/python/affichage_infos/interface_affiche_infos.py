# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).


class InterfaceAfficheInfos(object):
    '''
    Interface utilis�e pour d�finir les m�thodes � red�finir
    '''

    def affiche_information(self, dt_custom_time):
        raise RuntimeError("M�thode non red�fini")

    def eteindre_ecran(self):
        raise RuntimeError("M�thode non red�fini")


class InterfaceAfficheHorairesBus(object):
    '''
    Interface utilis�e pour d�finir les m�thodes � red�finir
    '''

    def set_dict_horaires(self, dict_horaires):
        '''
        Prend le dictionnaire et le converti dans le format interne pour le processus d'affichage

        :param dict_horaires:
        '''
        raise RuntimeError("M�thode non red�fini")


class InterfaceAfficheTemperatures(object):
    '''
    Interface utilis�e pour d�finir les m�thodes � red�finir
    '''

    def set_dict_temperatures(self, p_list_temp):
        '''
        :param p_dict_temperature:
                    ex : {"TEMP1":[6, datetime.datetime], "TEMP2":[6, datetime.datetime]} 
        '''
        raise RuntimeError("M�thode non red�fini")
