# -*- coding: latin-1 -*-
"""
@author: jingl3s

"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import logging

from lanceur.fonctions import calcul_temps_attente


class Stm2:
    def __init__(self):
        self._dict_horaires_bus = dict()
        self._stm = None
        self._logger = logging.getLogger(self.__class__.__name__)
        self._temps_decalage_acces_arret = 0

    def set_stm_manager(self, stm_manager):
        self._stm = stm_manager
        if 0 != self._temps_decalage_acces_arret:
            self._stm.set_temps_decalage(self._temps_decalage_acces_arret)

    def set_temps_decalage(self, temps):
        self._temps_decalage_acces_arret = temps
        if self._stm is not None:
            self._stm.set_temps_decalage(temps)

    def get_horaires(self, dt_heure_courante):
        self._dt_heure_courante = dt_heure_courante

        self._dict_horaires_bus = self._stm.get_prochain_horaires(self._dt_heure_courante)

        if "ERR" in self._dict_horaires_bus:
            self._logger.error("Aucun bus trouve.")
            self._logger.debug(self._dict_horaires_bus)
        else:
            self._suppression_anciens()

        self._logger.debug(self._dict_horaires_bus)

        return self._dict_horaires_bus

    def _suppression_anciens(self):
        """
        Suppression des plus vieux elements en veillant a ne pas avoir
        """
        for index, horaire in enumerate(self._dict_horaires_bus["HORAIRE"]):
            if horaire.is_temps_attente_less_than(self._temps_decalage_acces_arret):
                del self._dict_horaires_bus["HORAIRE"][index]
