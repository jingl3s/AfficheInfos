# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

from datetime import datetime
from math import log
import os
import platform
import psutil


def do_nothing(obj):
    pass


byteunits = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')


def filesizeformat(value):
    exponent = int(log(value, 1024))
    return "%.1f %s" % (float(value) / pow(1024, exponent), byteunits[exponent])


def bytes2human(n):
    """
    >>> bytes2human(10000)
    '9K'
    >>> bytes2human(100001221)
    '95M'
    """
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = int(float(n) / prefix[s])
            return '%s%s' % (value, s)
    return "%sB" % n


def cpu_usage():
    # load average, uptime
    av1, av2, av3 = os.getloadavg()
    return "LOAD: %.1f %.1f %.1f" \
        % (av1, av2, av3)


def cpu_temperature():
    tempC = ((int(open('/sys/class/thermal/thermal_zone0/temp').read()) / 1000))
#     return "CPU TEMP: %sc" % (str(tempC))
    return "CPU TEMP: {0:0.1f} C".format(tempC)


def mem_usage():
    usage = psutil.virtual_memory()
    return "MEM FREE: %s/%s" \
        % (bytes2human(usage.available), bytes2human(usage.total))


def disk_usage(p_dir):
    usage = psutil.disk_usage(p_dir)
    return "DSK FREE: %s/%s" \
        % (bytes2human(usage.total - usage.used), bytes2human(usage.total))


def network(iface):
    stat = psutil.net_io_counters(pernic=True)[iface]
    return "NET: %s: Tx%s, Rx%s" % \
           (iface, bytes2human(stat.bytes_sent), bytes2human(stat.bytes_recv))


def lan_ip():
    #f = os.popen('ifconfig eth0 | grep "inet\ addr" | cut -c 21-33')
    f = os.popen("ip route get 1 | awk '{print $NF;exit}'")
    ip = str(f.read())
    # strip out trailing chars for cleaner output
    return "IP: %s" % ip.rstrip('\r\n').rstrip(' ')


def platform_info():
    return "%s %s" % (platform.system(), platform.release())


def uptime():
    # La commande n'est pas disponible avec le meme nom sur toutes les
    # plateformes
    if 'boot_time' in dir(psutil):
        uptime = datetime.now() - datetime.fromtimestamp(psutil.boot_time()  # @UndefinedVariable
                                                         )
    else:
        uptime = datetime.now() - datetime.fromtimestamp(psutil.get_boot_time())
    return "Uptime %s" % str(uptime).split('.')[0]


def date():
    return str(datetime.now().strftime(
        '%a %b %d %H:%M:%S'))


def get_dht(dht_sensor):
    temp, hum = dht_sensor.get_valeurs()
    sensor = dht_sensor.get_sensor()
    return 'DHT{}: {:5.1f}°  {:4.1f}%'.format(sensor,
                                               temp, hum)
