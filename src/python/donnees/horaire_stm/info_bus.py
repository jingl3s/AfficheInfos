# -*- coding: latin-1 -*-
"""

@author: jingl3s

"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import pprint
import datetime
import logging


class InformationBus(object):
    """
    Classe format pour sauvegarder les informations de bus
    """

    def __init__(self, bus_id, horaire_formate, temps_attente=0, temps_attente_reel="", p_dt_heure_courante=None, p_is_error_msg=False):
        """
        Constructor
        """
        self.bus_id = bus_id
        self.horaire_formate = horaire_formate
        self.temps_attente = int(temps_attente)
        self.temps_attente_reel = temps_attente_reel
        if temps_attente != "" and p_dt_heure_courante is not None:
            self.horaire_dt = p_dt_heure_courante + datetime.timedelta(minutes=self.temps_attente)
        else:
            self.horaire_dt = None
        self.extra = dict()
        self._pp = pprint.PrettyPrinter(indent=4)
        self.fields_compare = list()
        self._logger = logging.getLogger(self.__class__.__name__)
        self._is_error_msg = p_is_error_msg

    def set_temps_attente(self, temps_attente):
        self.temps_attente = int(temps_attente)

    def update_temps_attente(self, p_dt_heure_courante):
        try:
            if self.horaire_dt is not None:
                minute_pour_dernier_bus = (self.horaire_dt - p_dt_heure_courante).seconds / 60
            else:
                # Conversion vers format datetime
                dt_heure_dernier_bus = datetime.datetime.strptime(self.horaire_formate, "%H:%M")

                # Calcul difference en secondes
                minute_pour_dernier_bus = (dt_heure_dernier_bus - p_dt_heure_courante).seconds / 60

                # Calcul specifique pour changement de jour
                if minute_pour_dernier_bus > (24 * 60 * 60):
                    minute_pour_dernier_bus = (
                        dt_heure_dernier_bus - datetime.datetime.strptime("00:00", "%H:%M")
                    ).seconds / 60
                self.horaire_dt = p_dt_heure_courante + datetime.timedelta(minutes=minute_pour_dernier_bus)
                

            self.temps_attente = int(minute_pour_dernier_bus)

        except ValueError as e:
            self._logger.error(e)

    def __str__(self, *args, **kwargs):

        return "'B:{}, H:{}, W:{}, WRT:{}, EXT:{}'".format(
            self.bus_id, self.horaire_formate, self.temps_attente, self.temps_attente_reel, self._pp.pformat(self.extra)
        )

    def __repr__(self, *args, **kwargs):
        return self.__str__()

    def add_extra_time(self, fieldname, dt_time, time_left):
        if fieldname in self.extra:
            self.extra[fieldname] = time_left
            self.extra[fieldname + "_dt"] = dt_time
        else:
            self.extra[fieldname] = time_left
            self.extra[fieldname + "_dt"] = dt_time
            self.fields_compare.append(fieldname)

    def add_extra_data(self, fieldname, data):
        self.extra[fieldname] = data

    def is_temps_attente_less_than(self, minutes):
        if self._is_error_msg:
            return False
        
        comparison_result = True
        for fieldname in self.fields_compare:
            if not fieldname.endswith("_dt"):
                comparison_result = self.extra[fieldname] < minutes
                # end loop as a result found
                break
        if self.temps_attente_reel != "":
            comparison_result &= abs(self.temps_attente_reel) < minutes
        comparison_result &= self.temps_attente < minutes
        return comparison_result

    def get_horaire_formate(self):
        for fieldname in self.fields_compare:
            if not fieldname.endswith("_dt"):
                return "{}:{:02d}".format(self.extra[fieldname + "_dt"].hour, self.extra[fieldname + "_dt"].minute)
        if self.horaire_dt is not None:
            return "{}:{:02d}".format(self.horaire_dt.hour, self.horaire_dt.minute)
        else:
            return self.horaire_formate

    def __lt__(self, to_compare):
        found, comparison_result = self._compare_fields(to_compare)

        if not found:
            if self.temps_attente_reel != "" and to_compare.temps_attente_reel != "":
                # Use of absolute as we use - as information
                comparison_result = abs(self.temps_attente_reel) < abs(to_compare.temps_attente_reel)
                found = True
            elif self.temps_attente_reel != "":
                comparison_result = abs(self.temps_attente_reel) < to_compare.temps_attente
                found = True
            elif to_compare.temps_attente_reel != "":
                comparison_result = self.temps_attente < to_compare.temps_attente_reel
                found = True

        if not found:
            comparison_result = self.temps_attente < to_compare.temps_attente
        return comparison_result

    def _compare_fields(self, to_compare):
        found = False
        comparison_result = False
        for fieldname in self.fields_compare:
            if fieldname + "_dt" in self.extra and fieldname + "_dt" in to_compare.extra:
                comparison_result = self.extra[fieldname + "_dt"] < to_compare.extra[fieldname + "_dt"]
                found = True
                # end loop as a result found
                break
            elif fieldname + "_dt" in self.extra:
                if to_compare.temps_attente_reel != "":
                    comparison_result = self.extra[fieldname] < to_compare.temps_attente_reel
                    found = True
                    break
                else:
                    comparison_result = self.extra[fieldname] < to_compare.temps_attente
                    found = True
                    break
        if not found:
            for fieldname in to_compare.fields_compare:
                if fieldname + "_dt" in to_compare.extra:
                    if self.temps_attente_reel != "":
                        comparison_result = abs(self.temps_attente_reel) < to_compare.extra[fieldname]
                        found = True
                        break
                    else:
                        comparison_result = self.temps_attente < to_compare.extra[fieldname]
                        found = True
                        break
        return found, comparison_result

