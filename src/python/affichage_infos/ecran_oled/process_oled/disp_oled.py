# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

from PIL import ImageFont
import os
import time
import subprocess
from collections import OrderedDict
from builtins import range


try:
    from pyA20.gpio import gpio as GPIO  # @UnresolvedImport
    from pyA20.gpio import port  # @UnresolvedImport
    MODE = "PYA20"
except (ImportError, ModuleNotFoundError):
    MODE = "RPI"
    try:
        import RPi.GPIO as GPIO

    except (ImportError, ModuleNotFoundError, RuntimeError):
        import GPIOEmu as GPIO

from luma.core.render import canvas  # @NoMove @UnresolvedImport
# from demo_opts import get_device



global width
width = 128
global height
height = 64

global col1
col1 = 0

global number_line_per_page
number_line_per_page = 4

# Screen rotation
g_rotate_val = 0

NBRE_LOOP = 6
global count_loop
count_loop = 6


def do_nothing(obj):
    pass


def make_font(name, size):
    font_path = os.path.abspath(os.path.join(
        os.path.dirname(__file__), 'fonts', name))
    return ImageFont.truetype(font_path, size)


def _get_font(p_s_text,  min_hauteur, limit_hauteur, _fichier_font, p_i_limite_longueur=0):
    font_valide = False
    font_size = 12
    _width = p_i_limite_longueur
    # desactivation longueur affichage automatique
    if 0 == p_i_limite_longueur:
        b_long = True
    else:
        b_long = False
    longueur_depasse = 0
    hauteur_depasse = 0
    derniere_longeur = -1
    print(p_s_text)
    last_size = -1

    while not font_valide or last_size != font_size:
        font = ImageFont.truetype(_fichier_font, font_size)
        last_size = font_size

        text_longeur = font.getsize(p_s_text)[0]
        text_hauteur = font.getsize(p_s_text)[1]

    #    print("font size:{}".format(font_size))
        # print("Longeur longeur:{}, hauteur:{}, posy:{}, longueur_depasse{}".format(
        #   text_longeur, text_hauteur, pos_haut, longueur_depasse))

        # Definir la taille de la police en fonction de la longueur de
        # l'écran
        if not b_long:
            if text_hauteur >= limit_hauteur:
                b_long = True
            elif text_longeur > (_width):
                font_size -= 1
                longueur_depasse += 1
                print("-l")
                b_long = True
            elif text_longeur < (_width) and longueur_depasse == 0:
                print("+l")
                font_size += 1
            else:
                b_long = True

     #   print("font size:{}".format(font_size))
        if 0 != p_i_limite_longueur and b_long:
            font_valide = True

        # Recherche haiteur optimale
        if b_long:
            if not font_valide:
                if text_hauteur > limit_hauteur:
                    font_size -= 1
                    hauteur_depasse += 1
                    print("-")
                # definir au moins hauteur min
                elif (text_hauteur < min_hauteur or text_hauteur < limit_hauteur)and 0 == hauteur_depasse:
                    font_size += 1
                    print("+")
                else:
                    if 0 == p_i_limite_longueur:
                        font_valide = True
        derniere_longeur = text_longeur

 #       print("blong:{}, font valide:{}, limit_hauteur:{}, haut depasse:{}".format(
  #          b_long, font_valide, limit_hauteur, hauteur_depasse))
    print("Final longeur:{}, hauteur:{}".format(
        text_longeur, text_hauteur))
    return font


def get_next_font():
    global list_font
    global counter
    counter = (counter + 1) % len(list_font)

    print(list_font[counter].getname())

    return list_font[counter]


def disp_text(device, p_list_text):
    '''
    Display the content of list to the device 
    :param device: Luma device
    :param p_list_text: List of text with a string per line to display
    '''
    global font10
    with canvas(device) as draw:
        #         draw.rectangle((0, 0, 127, height - 1), outline="white", fill="black")

        for line, text in enumerate(p_list_text):
            if line < len(line_pixel):
                draw.text((col1, line_pixel[line]), text,
                          font=font10, fill=255)


def get_oled_device(rotate_val):
    '''
    Intialize my dedicated reset of device to start properly on I2C

    :param rotate_val: The rotate vale expected by the device 0,1,2,3
    :return device: Luma device
    '''
    from luma.core.interface.serial import i2c  # @UnresolvedImport
    from luma.oled.device import ssd1306  # @UnresolvedImport

    # Activation du reset pour avoir l'affichage fonctionnel
    # initialize GPIO
    pin = port.PA6
    GPIO.init()  # @UndefinedVariable
    GPIO.setcfg(pin, GPIO.OUTPUT)  # @UndefinedVariable
    # send initial high, low, high
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.02)
    GPIO.output(pin, GPIO.LOW)
    time.sleep(0.02)
    GPIO.output(pin, GPIO.HIGH)
    time.sleep(0.02)

    # rev.1 users set port=0
    serial = i2c(port=0, address=0x3D)

    # rotate=2 180°
    device = ssd1306(serial, rotate=rotate_val)

    # Activer la ligne suivante permet de bloquer l'affichage sur le dernier text lors de l'arret du programme
    #     device.cleanup = do_nothing
    return device


def get_my_device(rotate=False):
    '''
    Try getting physical device otherwize return the emulated one
    :param rotate:True to enable rotation screen since last call of this function False stay as default
    :return device as luma
    '''
    global g_rotate_val
    if rotate:
        if g_rotate_val == 0:
            g_rotate_val = 2
        else:
            g_rotate_val = 0
    else:
        g_rotate_val = 0

    try:
        dev = get_oled_device(g_rotate_val)
    except:
        dev = get_emul_device(g_rotate_val)
    return dev


def get_emul_device(rotate_val):
    '''
    Create a luma emulator device
    :param rotate_val: The rotate vale expected by the device 0,1,2,3
    :return device as luma
    '''
    from luma.emulator.device import pygame  # @UnresolvedImport
    device = pygame(mode="1", rotate=rotate_val)
    # Activer la ligne suivante permet de bloquer l'affichage sur le dernier text lors de l'arret du programme
#     device.cleanup = do_nothing
    return device


def get_push_button(p_dict_pin):
    disp = list()
    for pin, pin_info in p_dict_pin.items():
        disp.append("{} {}".format(pin, pin_info["last_state"]))
    return "Push But: " + ",".join(disp)


def menu(lmenu, device=None, sous_menu=False, gpio_pins=None):
    '''
    Display a menu to the user and add a dedicated character in order to identify the current activated menu

    The device is used as parameter to display the menu content


    :param lmenu: List of menu to display
    :param device: Luma device for display
    :param sous_menu: If True, the sub menu to confirm is not displayed otherwise always disabled
    :param gpio_pins: dictionary of GPIO pins to use 
    '''

    # Copy the menu as modified to display the prompt
    smenu = lmenu[:]
    sel = 0
    eval_ret = None

    # Wait at least a selection in menu is confirmed
    while sel < len(lmenu) and eval_ret is None:

        # Assure de toujours afficher le contenu du menu meme si plusieurs
        # pages possibles
        if sel >= number_line_per_page:
            smenu = smenu[number_line_per_page - 1:]
            sel = sel - number_line_per_page + 1

        # Add the user prompt to identify menu position
        smenu[sel] = '>' + smenu[sel]

        disp_text(device, smenu)

        # attente de saisi utilisateur car menu demande
        val_lu = action_utilisateur(gpio_pins)

        # Goes next position in the menu
        if val_lu == True:
            sel = (sel + 1) % len(smenu)

        # Activate selection of menu
        elif val_lu == False:

            #             print('Menu selectionne : {}'.format(smenu[sel][1:]))

            # Ajout d'une demamde de confirmation pour valider la selection'
            if not sous_menu:
                confirm = list()
                confirm.append('annuler')
                confirm.append('confirmer')

                eval_ret1 = menu(confirm, device, True, gpio_pins=gpio_pins)

                # modification ret seulement si confirme
                if eval_ret1 == 1:
                    eval_ret = sel
            else:
                eval_ret = sel
        # Restore default menu as copy
        smenu = lmenu[:]
    return eval_ret


def action_push_button(pin_info, pins_complete):
    '''
    Function called when the GPIO change the current state in order to process something

    It will display a menu to user as the gpio button are use to go across the menu and validate with only two buttons
    :param pin_info: The current dictionary information of key pressed
    :param pins_complete: All the dictionary of gpio configured
    '''
    global device
    if pin_info["count"] == 2:

        dmenu = OrderedDict()
        dmenu['Eteindre'] = ['sudo', 'halt']
        dmenu['Redemarre'] = ['sudo', 'reboot']
#         dmenu['Test'] = 'print(\'test\')'
        dmenu['Tourner'] = ''
#         dmenu['Affiche tout'] = ''
        dmenu['Quitter menu'] = '0'

        lmenu = list(dmenu.keys())

        pin_info["count"] = 0
        pin_info["change"] = False

        index = menu(lmenu, device, gpio_pins=pins_complete)
        if index == lmenu.index('Eteindre') or index == lmenu.index('Redemarre'):
            # Shutdown screen before
            device.cleanup()
            # Execution commande
            print("Command to execute: {}".format(dmenu[lmenu[index]]))
            subprocess.call(dmenu[lmenu[index]])
        if index == lmenu.index('Tourner'):
            device = get_my_device(rotate=True)
#         if index == lmenu.index('Test'):
            # For information other idea to evaluate python command
#             eval_ret = eval(dmenu[lmenu[index]])  # @UnusedVariable

        pin_info["change"] = False
        pin_info["count"] = 0


def action_utilisateur(gpio_pin):
    '''
    Wait until a gpio changes in order to return the status change
    Infinite loop as user action request

    :param gpio_pin:
    :return: True if first gpio key is pressed and False otherwise
    '''
    retour = None
    cle_inter = list(gpio_pin.keys())

    while retour is None:
        io_verif_status(gpio_pin, desative_fonction=True)
        for index, gpio_key in enumerate(cle_inter):
            if gpio_pin[gpio_key]["change"]:
                retour = not (index)
        time.sleep(0.05)
    return retour


def io_setup(p_list_pin):
    # Activation du reset pour avoir l'affichage fonctionnel

    # initialize GPIO
    dict_pin = dict()

    if MODE == "PYA20":
        GPIO.init()  # @UndefinedVariable
    elif MODE == "RPI":
        GPIO.setmode(GPIO.BCM)

    for pin in p_list_pin:

        if MODE == "PYA20":
            if pin == 15:
                pa_pin = port.PA15
            elif pin == 16:
                pa_pin = port.PA16
            else:
                raise ValueError("Pin number is not expected {}".format(pin))

            GPIO.setcfg(pa_pin, GPIO.INPUT)  # @UndefinedVariable
            GPIO.pullup(pa_pin, GPIO.PULLUP)  # @UndefinedVariable
        elif MODE == "RPI":
            pa_pin = pin
            GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        else:
            raise ValueError("Pin number is not expected {}".format(pin))

        dict_pin[pin] = dict()
        dict_pin[pin]["pin"] = pa_pin

        current_state = GPIO.input(pin)
        dict_pin[pin]["last_state"] = current_state
        dict_pin[pin]["change"] = False
        dict_pin[pin]["count"] = 0

    return dict_pin


def io_verif_status(p_dict_pin, desative_fonction=False):
    '''
    Detect the button press and call the associated function if any
    :param p_dict_pin:
    :param desative_fonction:
    '''
    # Activation du reset pour avoir l'affichage fonctionnel
    action_released = False
    for pin_info in p_dict_pin.values():
        last_state = pin_info["last_state"]
        current = GPIO.input(pin_info["pin"])

        pin_info["last_state"] = current

        if not current and last_state != current:
            pin_info["change"] = True
            pin_info["count"] += 1
            if not desative_fonction:
                if pin_info["fonction"] is not None:
                    pin_info["fonction"](pin_info, p_dict_pin)
                    action_released = True
        else:
            pin_info["change"] = False

    return action_released


def initialize():
    global looper
    global line_pixel
    global page_lines
    global current_line
    global number_line_per_page
    global font10
    global device
    global dict_pin
    global list_font
    global counter

    looper = 0

    # Function available from Luma.demo demo_opts.py
    device = get_my_device()

    # Identify of embedded platform
    p_list_pin = list()
#     hostname = socket.gethostname()
    hostname = "localhost"
    if hostname in "localhost":
        p_list_pin.append(15)
        p_list_pin.append(16)
        dict_pin = io_setup(p_list_pin)
        dict_pin[15]["fonction"] = action_push_button
        dict_pin[16]["fonction"] = None
    else:
        dict_pin = dict()

    current_line = 0

    line_pixel = list()
    hauteur_ligne = int(height / number_line_per_page) - 1

    line_pixel.append(0)
    for cpt in range(1, number_line_per_page):
        line_pixel.append(cpt * hauteur_ligne)

    text_ref = "22-24-30-38".zfill(12).center(12)
    _repertoire_polices = os.path.join(os.path.dirname(__file__),  'fonts')
    _repertoire_polices = os.path.realpath(_repertoire_polices)
    cont = os.listdir(_repertoire_polices)
    cont_filtre = [
        fichier for fichier in cont if fichier.lower().endswith('ttf')]

    if 0 == len(cont_filtre):
        raise IOError("Missing font files in {}".format(_repertoire_polices))

    list_font = list()
    counter = 0
    for fichier in cont_filtre:
        fichier_ttf = os.path.join(_repertoire_polices, fichier)
        font_to_use = _get_font(
            text_ref, hauteur_ligne - 5, hauteur_ligne, fichier_ttf)
        list_font.append(font_to_use)

    looper = 1


def action_oled(list_text):
    global looper
    global line_pixel
    global page_lines
    global current_line
    global number_line_per_page
    global device
    global dict_pin
    global font10
    global count_loop

    io_verif_status(dict_pin)

    # Hide screen to save power
    if not dict_pin[16]["last_state"]:
        device.hide()
    else:
        # Display screen and update content
        device.show()

        if count_loop % NBRE_LOOP == 0:
            font10 = get_next_font()
            count_loop = 0

        # Modify content to add informations
        add_text = " " + font10.font.family[:2]
        if 2 >= len(list_text):
            list_text.append(add_text)
        else:
            l_text = list(list_text[2])
            l_text[9:12] = add_text
            list_text[2] = ''.join(l_text)

        disp_text(device, list_text)
        count_loop += 1
