# Require gtfs_realtime_bindings installed
# https://github.com/MobilityData/gtfs-realtime-bindings/blob/master/python/README.md
# from google.protobuf import timestamp_pb2
import datetime
import json
import logging
import pprint
from datetime import datetime as dt

import requests
from google.transit import gtfs_realtime_pb2


class StmApi:
    # URL = "https://api.stm.info/pub/od/gtfs-rt/ic/v2/tripUpdates/"
    URL = "https://api.stm.info/pub/od/gtfs-rt/ic/v2/tripUpdates"
    TIME_BETWEEN_UPDATES_MINUTES = 2

    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._feed = gtfs_realtime_pb2.FeedMessage()
        self._headers = None
        self._session = requests.Session()
        self._last_update = None
        self._state = True
        self._datetime_prochaine_mise_a_jour = dt.now()

    def set_json_config(self, p_config: dict):
        if "cle" not in p_config:
            raise ValueError("Missing cle field in config input fot STM api")
        self._headers = {"apikey": p_config["cle"], "accept": "application/x-protobuf"}

        # Parse config to extract bus and stops for filtering searchs
        self._stops_id = list()
        self._bus_settings = dict()

        for conf in p_config["CONF"]:
            list_bus = list()
            self._stops_id.append(conf["ARRET"])
            for bus_nb in conf["BUS"].keys():
                list_bus.append(bus_nb)

            self._bus_settings[conf["ARRET"]] = {
                "BUS": list_bus,
                "ARRIVALS": dict(),
                "ARRIVALS_FULL": dict(),
            }

    def update_stm_next_bus(self, current_date):

        self._logger.debug("Update stm api contact server")

        mise_a_jour = self._datetime_prochaine_mise_a_jour < current_date

        if mise_a_jour:
            try:
                response = self._session.get(self.URL, headers=self._headers)
                status = response.status_code
                content = response.content
            except:
                self._logger.exception("Error retrieve STM API")
                # return an error but continue processing
                status = 0
                content = ""

            if status == 200:
                self._update_with_answer(current_date, content)
                # Pour reduire le nombre d'appels du serveur entre 20h et 5h toutes les 2 minutes pour eviter depassement du bac a sable STM
                if (
                    current_date.hour > 20
                    or current_date.hour < 5
                    or (current_date.hour > 14 and current_date.hour < 16)
                ):
                    self._datetime_prochaine_mise_a_jour = current_date + datetime.timedelta(
                        minutes=self.TIME_BETWEEN_UPDATES_MINUTES
                    )
                else:
                    self._datetime_prochaine_mise_a_jour = current_date
            else:
                if status == 500:
                    self._logger.error(f"Erreur 500 observe {content}")
                self._remove_oldest(current_date)

        else:
            self._remove_oldest(current_date)

    def get_bus_update(self) -> dict:
        return self._bus_settings

    def _pprint(self):
        pp = pprint.PrettyPrinter(indent=4)
        self._logger.debug(pp.pformat(self._bus_settings))

    def _update_with_answer(self, current_date, web_answer):
        self._feed.ParseFromString(web_answer)
        first_loop = list()
        self._last_update = dt.now()
        for entity in self._feed.entity:
            if entity.HasField("trip_update"):

                var = entity.trip_update.stop_time_update
                for info in var:
                    if info.stop_id in self._stops_id:
                        if entity.trip_update.trip.route_id in self._bus_settings[info.stop_id]["BUS"]:
                            # print(info.schedule_relationship)
                            if info.HasField("arrival"):

                                if (
                                    entity.trip_update.trip.route_id
                                    not in self._bus_settings[info.stop_id]["ARRIVALS_FULL"]
                                    or f"{info.stop_id}_{entity.trip_update.trip.route_id}" not in first_loop
                                ):
                                    self._bus_settings[info.stop_id]["ARRIVALS_FULL"][
                                        entity.trip_update.trip.route_id
                                    ] = list()
                                    # self._bus_settings[info.stop_id]["ARRIVALS_FULL"][entity.trip_update.trip.route_id].append({"info": info, "trip": entity.trip_update.trip})
                                    self._bus_settings[info.stop_id]["ARRIVALS"][
                                        entity.trip_update.trip.route_id
                                    ] = list()
                                    first_loop.append(f"{info.stop_id}_{entity.trip_update.trip.route_id}")

                                ignored_trip = False

                                # Temps deja present
                                if (
                                    info.arrival.time
                                    in self._bus_settings[info.stop_id]["ARRIVALS"][entity.trip_update.trip.route_id]
                                ):
                                    val = self._bus_settings[info.stop_id]["ARRIVALS"][entity.trip_update.trip.route_id]
                                    self._logger.debug(
                                        f"=============================== Duplicate observed ==================== {info.arrival.time}: {val}"
                                    )

                                date_formated = dt.fromtimestamp(info.arrival.time)

                                if date_formated < current_date:
                                    self._logger.debug(
                                        f"=============================== time in the past ignored ==================== {info.arrival.time}: {entity.trip_update.trip}"
                                    )
                                    ignored_trip = True

                                if info.schedule_relationship != 0:
                                    self._logger.error(
                                        f"=============================== time not valid for this trip then ignored ==================== time: {info.arrival.time}: trip: {entity.trip_update.trip}, schedule_relationship: {info.schedule_relationship}"
                                    )
                                    ignored_trip = True

                                if not ignored_trip:
                                    self._bus_settings[info.stop_id]["ARRIVALS_FULL"][
                                        entity.trip_update.trip.route_id
                                    ].append(
                                        {
                                            "time": info.arrival.time,
                                            "timef": dt.fromtimestamp(info.arrival.time),
                                            # Scheduled 0, ADDED 1, UNSCHEDULED 2, CANCELED 3 , REPLACEMENT 5
                                            "schedule_relationship": entity.trip_update.trip.schedule_relationship,
                                        }
                                    )
                                    self._bus_settings[info.stop_id]["ARRIVALS"][
                                        entity.trip_update.trip.route_id
                                    ].append(info.arrival.time)

        # Add a sorting items to ensure they are in most recent first
        for bus_stop_info in self._bus_settings.values():
            if "ARRIVALS" in bus_stop_info:
                for bus_ligne_info in bus_stop_info["ARRIVALS"].values():
                    bus_ligne_info.sort()
        for bus_stop_info in self._bus_settings.values():
            if "ARRIVALS_FULL" in bus_stop_info:
                for bus_ligne_info in bus_stop_info["ARRIVALS_FULL"].values():
                    bus_ligne_info.sort(key=lambda x: x["time"])

    def _remove_oldest(self, current_date):
        times_to_del = list()

        for stop_info in self._bus_settings.values():
            del times_to_del[:]
            for bus_list in stop_info["ARRIVALS_FULL"].values():
                for index, current_planned in enumerate(bus_list):
                    if current_planned["timef"] < current_date:
                        times_to_del.append(current_planned["time"])
                        del bus_list[index]

            for time_info in times_to_del:
                for key, bus_list_arrivals in stop_info["ARRIVALS"].items():
                    try:
                        index_found = bus_list_arrivals.index(time_info)
                        if index_found >= 0:
                            del bus_list_arrivals[index_found]
                    except ValueError:
                        # Ignore value not define as could be the case
                        pass
        pass
