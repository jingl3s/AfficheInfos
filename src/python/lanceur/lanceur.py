# -*- coding: latin-1 -*-
"""
@author: jingl3s

Informations:
Manage Date format:
https://docs.python.org/3.5/library/datetime.html#timedelta-objects
http://stackoverflow.com/questions/14110927/python-datatime-time-and-arithmetic-operations#14110992
"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging
import os
import time

from affichage_infos.affichage_info import AfficheInfoFactory, Afficheurs
from donnees.domoticz.recupere_information import Capteur, Domoticz
from donnees.extra.extra import ExtraDonnees
from donnees.horaire_stm.manage_stm import ManageSTM
from donnees.horaire_stm.stm2 import Stm2
from donnees.horaire_stm.stm_api import StmApi
from donnees.horaire_stm.stm_api_retriever import StmApiRetriever
from donnees.taux_change.recupere_taux import RecupereTaux
from lanceur.processus_donnee import DonneeProcessus
from tools import convert_config_path


class Lanceur:
    TEMPS_ENTRE_AFFICHAGE = 60

    def __init__(self, p_s_dossier_travail):
        self._dt_heure_courante = None
        self._stm = None
        self._dossier_travail = p_s_dossier_travail
        self._afficheur = None
        self._temperatures = dict()
        self._previsions = None
        self._previsions_semaine = None
        self._change_argent = list()
        self._list_processus = list()
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(logging.INFO)
        self._temps_entre_affichage = self.TEMPS_ENTRE_AFFICHAGE

    @staticmethod
    def affichage_mode(p_mode, p_s_dossier_travail, p_clefs, p_config_mode, p_eteindre_ecran=True):
        ret_val = None
        lance = Lanceur(p_s_dossier_travail)

        if "temps_entre_rafraichissement" in p_config_mode:
            lance.set_temps_entre_affichage(p_config_mode["temps_entre_rafraichissement"])

        if p_mode == "WEB":
            lance.configure_mode_page_web(p_config_mode, p_eteindre_ecran, p_clefs)
            ret_val = lance
        elif p_mode == "STATIQUE":
            lance.configure_mode_page_web(p_config_mode, p_eteindre_ecran, p_clefs)
            lance.run()
        elif p_mode == "OLED":
            lance.configure_mode_page_oled(p_config_mode, p_eteindre_ecran, p_clefs)
            lance.run()
        else:
            pass
        return ret_val

    def _construire_objets_page_web(self, p_config_mode, p_eteindre_ecran, p_clefs, p_afficheur):
        # Objet : Afficheur informations
        afficheur_web = AfficheInfoFactory.get_afficheur(p_afficheur)
        afficheur_web.set_dossier_cible(self._dossier_travail)
        # Set the directory of resources to use
        afficheur_web.set_chemin_resources(
            os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "..", "resources")
        )
        afficheur_web.set_extinction_ecran(p_eteindre_ecran)
        # Sub directory of static resources to use
        afficheur_web.set_nom_dossier_ressources_utiliser(p_config_mode["rsrc_folder"])

        list_donnees = list()
        list_donnees.append("domoticz")
        list_donnees.append("stm")
        list_donnees.append("taux")
        list_donnees.append("extra")
        list_donnees.append("display_options")

        if "DONNEES" in p_config_mode:
            list_donnees = p_config_mode["DONNEES"]

        # Objet : Domoticz
        if "domoticz" in p_clefs and "domoticz" in list_donnees:
            self._recupere_temperatures = Domoticz()
            DOMOTICZ_SERVER_ADDRESS = "192.168.254.194:8080"
            domoticz_id_ext = 27
            domoticz_id_int = 10
            domoticz_id_hydro = 55
            domoticz_id_wifi = 43
            self._recupere_temperatures.set_domoticz_server_address(DOMOTICZ_SERVER_ADDRESS)
            #             self._recupere_temperatures.set_id_capteur(
            #                 Capteur.EXT, domoticz_id_ext)
            #             self._recupere_temperatures.set_id_capteur(
            #                 Capteur.INT, domoticz_id_int)
            #             self._recupere_temperatures.set_id_capteur(
            #                 Capteur.ELEC, domoticz_id_hydro)
            #             self._recupere_temperatures.set_id_capteur(
            #                 Capteur.E_RES, domoticz_id_ext)
            #             self._recupere_temperatures.set_id_capteur(
            #                 Capteur.E_EOL, 62)
            self._recupere_temperatures.set_id_capteur(Capteur.I_WIFI, domoticz_id_wifi)
            #             self._recupere_temperatures.set_id_capteur(
            #                 Capteur.E_HUM, 63)

            proc = DonneeProcessus("domoticz", dict())
            proc.set_fct_lecture(self._recupere_temperatures.get_toutes_temperatures)
            proc.set_fct_envoi(afficheur_web.set_dict_temperatures)
            self._list_processus.append(proc)

        # Objet : STM
        if "stm" in p_clefs and "stm" in list_donnees:
            dossier_trouve = ""

            stm_api_agent = StmApi()
            stm_api_agent.set_json_config(p_clefs["stm"])

            proc = DonneeProcessus("stm-api-{}".format(0), dict())
            proc.set_fct_lecture(stm_api_agent.update_stm_next_bus)
            self._list_processus.append(proc)

            for index, stm_config in enumerate(p_clefs["stm"]["CONF"]):
                stm = ManageSTM()

                if dossier_trouve == "":
                    dossier_trouve = self._extract_stm_db_directory(p_clefs["stm"]["DB_PATHS"], stm)
                    self._logger.info(f"Dossier DB: {dossier_trouve}")
                else:
                    stm.set_stm_data_dir(dossier_trouve)
                stm.set_json_config(stm_config)

                stm_api_retrieve = StmApiRetriever()
                stm_api_retrieve.set_stm_api(stm_api_agent)
                stm_api_retrieve.set_json_config(stm_config)

                stm.set_stm_api_extra(stm_api_retrieve)

                stm_g = Stm2()
                stm_g.set_stm_manager(stm)
                stm_g.set_temps_decalage(p_clefs["stm"]["temps_decalage"])

                proc = DonneeProcessus("stm-{}".format(index), dict())
                proc.set_fct_lecture(stm_g.get_horaires)
                proc.set_fct_envoi(afficheur_web.set_dict_horaires_multi, index)
                self._list_processus.append(proc)

        # Objet : Taux change
        if "taux" in p_clefs and "taux" in list_donnees:
            self._taux_change = RecupereTaux()
            self._taux_change.set_alpha_key(p_clefs["taux"]["alphavantage"]["cle"])

            proc = DonneeProcessus("taux", dict())
            proc.set_fct_lecture(self._taux_change.obtenir_taux)
            proc.set_fct_envoi(afficheur_web.set_taux)
            self._list_processus.append(proc)

        # Objet : Extras
        self._extras = ExtraDonnees()
        if "extra" in p_clefs and "extra" in list_donnees:

            proc = DonneeProcessus("extra", dict())
            proc.set_fct_lecture(self._extras.obtenir_donnees)
            proc.set_fct_envoi(afficheur_web.set_data_with_key, "extra")
            self._list_processus.append(proc)

        # Objet : Time extra option
        display_time = False
        full_screen_button = False
        if "display_options" in p_clefs and "display_options" in list_donnees:
            if "time_display" in p_clefs["display_options"]:
                display_time = p_clefs["display_options"]["time_display"] == "true"
            if "full_screen_button" in p_clefs["display_options"]:
                full_screen_button = p_clefs["display_options"]["full_screen_button"] == "true"

        afficheur_web.set_data_with_key(display_time, "time")
        afficheur_web.set_data_with_key(full_screen_button, "full_screen_button")

        return afficheur_web

    def configure_mode_page_web(self, p_config_mode, p_eteindre_ecran, p_clefs):
        self._afficheur = self._construire_objets_page_web(
            p_config_mode, p_eteindre_ecran, p_clefs, Afficheurs.PAGE_WEB
        )

    def configure_mode_page_oled(self, p_config_mode, p_eteindre_ecran, p_clefs):
        self._afficheur = self._construire_objets_page_web(
            p_config_mode, p_eteindre_ecran, p_clefs, Afficheurs.PAGE_OLED
        )

    def run(self):
        # Date et heure actuelle
        # source :
        # http://www.saltycrane.com/blog/2008/06/how-to-get-current-date-and-time-in/

        boucle_infinie = True
        self._logger.debug("boucle")

        while boucle_infinie:

            self._mise_a_jour_donnees()

            self._logger.debug("Heure courante:{}".format(self._dt_heure_courante.strftime("%H:%M")))

            self._affichage_envoi_donnees()

            self._afficheur.affiche_information(self._dt_heure_courante)

            self._afficheur.eteindre_ecran(self._dt_heure_courante)

            # Attente de 1 minute avant prochain enregistrement depuis le
            # debut de l'execution

            calcul = self._temps_entre_affichage - (datetime.datetime.now() - self._dt_heure_courante).seconds
            if calcul > 0:
                time.sleep(calcul)

    def obtenir_contenu(self):
        self._mise_a_jour_donnees()

        self._logger.debug("Heure courante:{}".format(self._dt_heure_courante.strftime("%H:%M")))

        self._affichage_envoi_donnees()

        return self._afficheur.affiche_obtenir_contenu(self._dt_heure_courante)

    def _affichage_envoi_donnees(self):
        for proc in self._list_processus:
            try:
                proc.envoi_valeur()
            except Exception as e:
                self._logger.exception("erreur mise a jour")
                self._logger.debug(e)
                # ignore exception car on veut tout traiter

    def _mise_a_jour_donnees(self):
        self._dt_heure_courante = datetime.datetime.now()

        for proc in self._list_processus:
            try:
                proc.mise_a_jour(self._dt_heure_courante)
            except Exception as e:
                # ignore exception car on veut tout traiter
                self._logger.error("Exception detecte le programme continue: ", e)

    def set_temps_entre_affichage(self, p_temp_affichage_seconds: int):
        self._temps_entre_affichage = p_temp_affichage_seconds

    def _extract_stm_db_directory(self, p_db_paths, stm):
        """
        Fomr list of path identify the valid one by using current stm object to verify if the directory is valid
        :param p_db_paths:
        :param stm:
        """
        dossier_trouve = ""

        list_dossiers_db = convert_config_path.ConvertConfigPath.convert(os.path.dirname(__file__), p_db_paths)

        # Check each directory if already valid for stm static usage
        for current_directory in list_dossiers_db:
            self._logger.debug("Detection dossier : {}".format(current_directory))
            if os.path.exists(current_directory) and dossier_trouve == "" and len(os.listdir(current_directory)) >= 1:
                if stm.set_stm_data_dir(current_directory):
                    dossier_trouve = current_directory

        # Take first directory that can be created if no one is already valid
        # for stm
        if "" == dossier_trouve:
            for current_directory in list_dossiers_db:
                if os.path.exists(current_directory):
                    dossier_trouve = current_directory
                    break
            if "" == dossier_trouve:
                for current_directory in list_dossiers_db:
                    if not os.path.exists(current_directory):
                        try:
                            os.mkdir(current_directory)
                        except FileNotFoundError:
                            # Go to next directory
                            pass
                        finally:
                            dossier_trouve = current_directory
                            break

        if "" != dossier_trouve:
            self._logger.debug("db stm selected {}".format(dossier_trouve))
        else:
            self._logger.error("No STM directory found for database file")

        return dossier_trouve
