#/usr/bin/bash

lCheminTravail=$(cd `dirname $0` && pwd) >/dev/null
cd $lCheminTravail

while [ 1 ]
do
    git_before=`git log -n 1 --pretty=format:%d`
    git fetch > /dev/null
    git pull origin > /dev/null 2>/dev/null
    git_after=`git log -n 1 --pretty=format:%d`

    diff <(echo $git_before) <(echo $git_after) > /dev/null

    if [ $? -ne 0 ]; then
        echo $(date): Restart web server
        bash termux_affich_info_web.sh
    fi
    sleep 85000
done

cd - > /dev/null