#/usr/bin/bash

pid=$(ps -ef |grep python3|grep affiche_info_conf.py|cut -f 1 -d ' ')

echo $pid|egrep '^[0-9]+$'
if [ $? -eq 0 ]; then
    kill $pid
else
    pid=$(ps -ef |grep python3|grep affiche_info_conf.py|grep -v grep | awk '{print $2}')
    echo $pid|egrep '^[0-9]+$'
    if [ $? -eq 0 ]; then
        kill $pid
    fi
fi

# Detect working directory
dossier_android=$HOME/scripts/AfficheInfos/src/python
export PYTHONPATH=$dossier_android 
lCheminTravail=$(cd `dirname $0` && pwd)
dossier_dev=$lCheminTravail/../python/

if [ -d $dossier_android ] ;then
    dossier=$dossier_android
else
    dossier=$dossier_dev
fi


# Detect version of python
var=$(which python3|wc -l)
if [ $var==1 ] ; then
    pyth=python3
else
    pyth=python
fi
cd $dossier_android/tests
$pyth test_manage_stm.py

cd -

exit
