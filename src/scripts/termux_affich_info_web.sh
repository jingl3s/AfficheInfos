#/usr/bin/bash


# First try termux way as pid is first and not second as on debian
pid=$(ps -ef |grep python3|grep affiche_info_conf.py|sed -e's/  */ /g'|cut -f 1 -d ' ')
echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Affiche info detruit"
else
    pid=$(ps -ef |grep python3|grep affiche_info_conf.py|grep -v grep | awk '{print $2}')
    echo $pid|egrep '^[0-9 ]+$' >/dev/null
    if [ $? -eq 0 ]; then
        kill $pid
    echo "Affiche info detruit"
    fi
fi


# First try termux way as pid is first and not second as on debian
pid=$(ps -ef |grep python3|grep background_update|sed -e's/  */ /g'|cut -f 1 -d ' ')
echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "background_update detruit"
else
    pid=$(ps -ef |grep python3|grep background_update|grep -v grep | awk '{print $2}')
    echo $pid|egrep '^[0-9 ]+$' >/dev/null
    if [ $? -eq 0 ]; then
        kill $pid
    echo "background_update detruit"
    fi
fi


PORT=8081

# Detect working directory
dossier_android=$HOME/scripts/AfficheInfos/src/python
lCheminTravail=$(cd `dirname $0` && pwd) >/dev/null
dossier_dev=$lCheminTravail/../python/

if [ -d $dossier_android ] ;then
    dossier=$dossier_android
    # As on android take wake lock on termux
    termux-wake-lock
else
    dossier=$dossier_dev
fi

config_file="config_web_hass.json"
if [ ! -z $USER ] ; then
    if [ "$USER" = "soniahugo"  ]  ; then
        config_file=""
    fi
fi
#termux-info |grep 'Device model' -A 1
enable_android=0
termux-sensor >/dev/null 2>/dev/null

if [ $? -eq 0 ] ; then
    #android device specif peocessing
    enable_android=1    

fi

# Start background update task if not started
exist=$(ps -ef |grep git_auto_update_restart.sh|grep -v grep|wc -l)
if [ $exist -eq 0 ] ; then
    echo Starting git_auto_update_restart.sh
    bash $dossier/../scripts/git_auto_update_restart.sh &
fi

# Detect version of python
var=$(which python3.7|wc -l)
if [ $var -eq 1 ] ; then
    pyth=python3.7
else
	var=$(which python3|wc -l)
	if [ $var -eq 1 ] ; then
		pyth=python3
	else
		pyth=python
	fi
fi

    echo "Utilisation config: $config_file"

cd $dossier
(
    $pyth affiche_info_conf.py WEB $config_file
) | (
    if [ "$config_file" != "config_web_hass.json" ] ; then
        sleep 3
        xdg-open http://0.0.0.0:$PORT/
    fi
    
    if [ $enable_android -eq 1 ] ; then
        cd extra
    #    $pyth background_update_wifi_schedule.py    &
        cd - >/dev/null
    fi
)

cd -  >/dev/null

exit
