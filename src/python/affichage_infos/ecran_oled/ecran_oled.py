# -*- coding: latin-1 -*-
"""
@author: jingl3s

"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging
import urllib
from urllib.error import URLError

from affichage_infos.interface_affiche_infos import (
    InterfaceAfficheInfos,
    InterfaceAfficheHorairesBus,
    InterfaceAfficheTemperatures,
)
from donnees.horaire_stm.info_bus import InformationBus
from affichage_infos.ecran_oled.process_oled.lit_dht import ReadDht

try:
    # Load the module but if not success does not use this feature
    from affichage_infos.ecran_oled.process_oled import disp_oled

    ecran_dispo = True
except:
    ecran_dispo = False


class EcranOLED(InterfaceAfficheInfos, InterfaceAfficheHorairesBus, InterfaceAfficheTemperatures):
    """
    Construction affichage pour les bus sur ecran OLED
    """

    def __init__(self):
        """
        RAS
        """
        self._heure_courante = 0
        self._affiche_navigateur = False
        self._logger = logging.getLogger(self.__class__.__name__)
        self._dernier_extinction_ecran = None
        self._list_temperatures = list()
        self._list_horaires = list()
        self._nbre_horaire = 4
        self._chemin_resources = None
        if ecran_dispo:
            disp_oled.initialize()
        self._sensor = ReadDht()
        self._data = dict()

    def set_dossier_cible(self, p_chemin_sortie):
        self._dossier_cible = p_chemin_sortie

    def set_config(self, p_config):
        pass

    def set_dict_temperatures(self, p_dict_temperature):
        """

        :param p_dict_temperature:
                    ex : {"TEMP1":[6, datetime.datetime], "TEMP2":[6, datetime.datetime]} 
        """
        self._prepare_elements_tableau_temperature(p_dict_temperature)

    def set_dict_horaires_multi(self, p_horaires: dict, p_index: int = 0):
        """
        Prend le dictionnaire et le converti dans le format interne pour le processus d'affichage

        :param p_horaires:
            ex : {TITRE: 'Snowdon', 'HORAIRE':[InformationBus:Object1, InformationBus:Object2 ]}
        """
        self._prepare_elements_tableau_horaire_multi(p_horaires, p_index)

    def set_data_with_key(self, p_valus: [dict, str], p_key: str):
        self._data[p_key] = p_valus

    def set_extinction_ecran(self, p_eteindre_ecran):
        self._extinction_ecran = p_eteindre_ecran

    def set_previsions(self, p_list_previsions):
        """

        :param p_list_previsions:
                    ex : [ {prevision1}, {prevision2} ] 
        """
        self._list_previsions = p_list_previsions

    def set_previsions_semaine(self, p_list_previsions_semaine):
        """

        :param p_list_previsions:
                    ex : [ {prevision1}, {prevision2} ] 
        """
        self._list_previsions_semaine = p_list_previsions_semaine

    def set_taux(self, p_list_taux):
        self._list_taux = p_list_taux

    def set_chemin_resources(self, p_chemin: str):
        self._chemin_resources = p_chemin

    def set_nom_dossier_ressources_utiliser(self, p_not_used):
        pass

    def affiche_information(self, dt_current_date_time):
        self._logger.debug("Affichage information ecran OLED !")
        self._heure_courante = dt_current_date_time

        self._format_display_OLED()

    def _format_display_OLED(self):
        """
        """
        dt_current_date_time = datetime.datetime.now()

        # TODO: Prendre en compte temperature
        # Specify any input variables to the template as a dictionary.
        #         templateVars = {
        #             "date_creation_fichier": dt_current_date_time.strftime("%d/%m/%Y %H:%M")}
        #
        #         if len(self._list_temperatures) > 0:
        #             templateVars["table_temperatures"] = self._list_temperatures
        # self._data
        # if 'extra' in self._data:
            # self._list_horaires.append(self._data['extra'])

        if 0 == len(self._list_horaires):
            self.disp_text([self._data['extra'][:15],self._data['extra'][:15],self._data['extra'][:15],self._data['extra'][:15]])
        else:
            self.disp_text(self._list_horaires)

    def disp_text(self, p_text):
        self._logger.debug(p_text)

        list_text = p_text[:]
        # Add dht infos
        if 3 == len(list_text):
            add_text = self._sensor.get_values()
            list_text.append(add_text)

        if ecran_dispo:
            disp_oled.action_oled(list_text[: self._nbre_horaire])
        self._affiche_text(list_text[: self._nbre_horaire])

    def eteindre_ecran(self, dt_heure_courante):
        """
        Non applicable OLED car desactive par interrupteur pour le moment
        :param dt_heure_courante:
        """
        #         if self._dernier_extinction_ecran is None:
        #             self._dernier_extinction_ecran = dt_heure_courante
        #
        #         # Calcul difference en secondes
        #         calcul = (
        #             ((dt_heure_courante - self._dernier_extinction_ecran).seconds) / 60.0)
        #         if calcul > 10:
        #             self._logger.debug("Extinction ecran")
        #             # Ins�rer ici l'appel � la m�thode de l'Afficheur pour �teindre l'�cran si mode facile pour r�activer
        #             self._logger.debug("Extinction ecran : fin")
        #             self._dernier_extinction_ecran = dt_heure_courante
        pass

    def _prepare_elements_tableau_horaire_multi(self, p_horaire, p_index):

        # Restriction seulement l'index 0 pour eviter de trop charger
        if p_index == 0:

            # Create default table if index does not exist
            # Fill it with content
            if len(p_horaire) > 0:

                del self._list_horaires[:]
                _list_inter = list()
                _list_inter_reel = list()

                self._list_horaires.append(p_horaire["TITRE"])
                for values in p_horaire["HORAIRE"]:

                    #                     bus_id = values.bus_id
                    #                     horaire_formate = values.horaire_formate
                    temps_attente = values.temps_attente
                    temps_reel = values.temps_attente_reel

                    if temps_reel != "":
                        if 0 > int(temps_reel):
                            temps_reel = str(-int(temps_reel))

                        _list_inter_reel.append("{:>2}".format(temps_reel))
                    # else:
                    _list_inter.append("{:>2}".format(temps_attente))

                    # End loop in case of sufficient values
                    if self._nbre_horaire == len(_list_inter_reel):
                        break
                    if self._nbre_horaire == len(_list_inter) and 0 == len(_list_inter_reel):
                        break

                # Manage cases not found in loop
                if 2 >= len(self._list_horaires):
                    # Add real time
                    if 0 < len(_list_inter_reel):
                        self._list_horaires.append("-".join(_list_inter_reel))
                        self._list_horaires[0] += " - TR"

                    # Add estimated in the list
                    if 0 < len(_list_inter):
                        if 2 == len(self._list_horaires):
                            self._list_horaires.append("EST")
                        else:
                            self._list_horaires[0] += " - EST"
                        # Restrict list to only 4 items
                        self._list_horaires.append("-".join(_list_inter[:4]))

                # Add current time
                dt_current_date_time = datetime.datetime.now()
                cur_time = dt_current_date_time.strftime("%H:%M")
                if 3 <= len(self._list_horaires):
                    self._list_horaires[2] += " {}".format(cur_time)
                else:
                    self._list_horaires.append(cur_time)

    def _prepare_elements_tableau_temperature(self, dict_temperature):

        if len(dict_temperature) > 0:
            del self._list_temperatures[:]
            for capteur in sorted(dict_temperature):
                if dict_temperature[capteur][0] is not None:
                    temperature = dict_temperature[capteur][0]
                    horaire_formate = dict_temperature[capteur][1].strftime("%d/%m/%Y %H:%M")

                    self._list_temperatures.append([capteur, horaire_formate, temperature])

    def _affiche_text(self, p_list_text):
        self._logger.debug(p_list_text)
        ip_address = "0.0.0.0"
        port_reseau = "8082"
        url_update_text = "http://{}:{}/text".format(ip_address, port_reseau)
        start_line = 0

        values_text = {"text": "", "add": "", "mod": "mod", "index": ""}

        last_comm_success = False

        for id, tt in enumerate(p_list_text, start_line):
            values_text["text"] = tt
            values_text["index"] = str(id)
            data_text = urllib.parse.urlencode(values_text)
            # data should be bytes
            data_text = data_text.encode("ascii")

            try:
                with urllib.request.urlopen(url_update_text, data_text) as response:
                    html = response.read()
                    if "error" in str(html).lower():
                        self._logger.error("com error")
                    else:
                        last_comm_success = True

            except URLError as e:
                self._logger.debug("Exception URLError {}".format(e))
                break
            except ConnectionRefusedError as e:
                self._logger.debug("Exception ConnectionRefusedError {}".format(e))
                break
        if last_comm_success:
            # Mise a jour affichage
            url_update = "http://{}:{}/mise_a_jour_images".format(ip_address, port_reseau)

            # Ajout valeurs pour etre sur d'utiliser un POST
            values = {}
            data = urllib.parse.urlencode(values)
            data = data.encode("ascii")  # data should be bytes

            try:
                with urllib.request.urlopen(url_update, data) as response:
                    html = response.read()
                    if "error" in str(html).lower():
                        pass
                    # Alternative pour avoir la r�ponse json
                    #         result = json.loads(response.readall().decode('utf-8'))
            except URLError as e:
                self._logger.error("Exception" + str(e))


if __name__ == "__main__":
    page = EcranOLED()
    #     page._heure_courante = datetime.datetime.now()
    dict_horaires_bus = {
        InformationBus(51, "21:55", "10"),
        InformationBus(51, "21:43", "5"),
        InformationBus(170, "21:07", "5"),
    }
    #     dict_horaires_bus = {}
    dict_temperatures = dict()
    dict_temperatures["T_EXT"] = [6, datetime.datetime.now()]
    dict_temperatures["T_INT"] = [23, datetime.datetime.now()]
    page.set_dict_temperatures(dict_temperatures)
    page.set_dict_horaires(dict_horaires_bus)
    page.affiche_information(datetime.datetime.now())
