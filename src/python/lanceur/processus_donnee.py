# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

class DonneeProcessus():
    def __init__(self, nom, donnee_init):
        self._nom = nom
        self._donnee = donnee_init
        self._fct_lecture = None
        self._fct_envoi = None
        self._fct_envoi_params = None

    def set_fct_lecture(self, fct):
        self._fct_lecture = fct

    def set_fct_envoi(self, fct, params=None):
        self._fct_envoi = fct
        self._fct_envoi_params = params

    def mise_a_jour(self, date):
        self._donnee = self._fct_lecture(date)

    def envoi_valeur(self):
        if self._fct_envoi is not None:
            if self._fct_envoi_params is None:
                self._fct_envoi(self._donnee)
            else:
                self._fct_envoi(self._donnee, self._fct_envoi_params)
