'''
@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import datetime
import logging
import os


class ExtraDonnees(object):
    '''
    Retrieve Extra data
    '''

    def __init__(self):
        '''

        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._derniere_lecture = ""

    def obtenir_donnees(self, p_datetime):
        val_lue = None
        try:
            val_lue = "{0}, {1}".format(self._lan_ip(), self._get_adresse_ip())

        except Exception as e:
            self._logger.error(
                "Erreur lecture information extras, execution continue {}".format(e))

        if val_lue is not None:
            self._derniere_lecture = val_lue

        return self._derniere_lecture

    def _lan_ip(self):
        ip_finale = ""
        with os.popen("ip route get 1 | awk '{print $NF;exit}'") as ip_addr_io:
            ip = str(ip_addr_io.read())
            # strip out trailing chars for cleaner output
            ip_finale = ip.rstrip('\r\n').rstrip(' ')
            
        return "IP: {}".format(ip_finale)

    def _get_adresse_ip(self):
        ip_address = ""
        with os.popen('ip addr show') as ip_addr_io:
            ip_address_read = ip_addr_io.read()
            if not ip_address_read in "":
                list_ip_address = ip_address_read.split("inet ")
                ip_address = list_ip_address[len(
                    list_ip_address) - 1].split('/')[0]

        return "IP: {}".format(ip_address)


if __name__ == "__main__":
    extra_data = ExtraDonnees()
    data_read = extra_data.obtenir_donnees(datetime.datetime.now())
    print(data_read)
