# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging
from requests import session

from bs4 import BeautifulSoup


class StmRealTime():

    '''
    StmRealTime: Classe pour acceder au site de Stm pour horaire en temps reel
    '''

    URL = "https://m.stm.info/fr/horaires/bus/{0}/arrets/{1}/arrivees?limit={2}&direction={3}"

    def __init__(self, p_test_mode=False):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)
#         self._logger.setLevel(logging.DEBUG)

        self._url_site = ""
        self._test_mode = p_test_mode

        self._bus = 51
        self._arret = 51176
        self._number_bus = 4
        self._direction = "West"

        # Header with personal information as idea from
        # http://robertorocha.info/on-the-ethics-of-web-scraping/
        self._header = {
            "host": "m.stm.info", "user-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:62.0) Gecko/20100101 Firefox/62.0;Hugo Carnide/Montreal/jingl3s@yopmail.com",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
            "accept-encoding": "gzip, deflate, br",
            "cookie": "betaOn=1; _stm-mobile_session=BAh7B0kiD3Nlc3Npb25faWQGOgZFRkkiJTIxNTZjNzcwZTc3ZmY2NWU5MmNmY2M2MDdhN2JjYjY0BjsAVEkiEF9jc3JmX3Rva2VuBjsARkkiMU9hNkgzeTZ2ZWpsNVNwajVhUTF3REN3YmcvelRCdXJJK2VJdkJEaEdBZkk9BjsARg%3D%3D--69199645cfcc83ae50f5448cccaf6c983fa527a1",
            "connection": "keep-alive",
            "upgrade-insecure-requests": "1",
            "pragma": "no-cache",
            "cache-control": "no-cache"}
        self._list_horaire = list()

    def set_ligne_bus(self, p_bus: int):
        self._bus = p_bus

    def set_arret(self, p_arret: int):
        self._arret = p_arret

    def set_nombre_horaire(self, p_number_bus):
        self._number_bus = p_number_bus

    def set_direction(self, p_direction):
        self._direction = p_direction

    def get_buses(self):
        '''

        '''
        if self._url_site == "":
            self._url_site = self.URL.format(
                self._bus,
                self._arret,
                self._number_bus,
                self._direction)

        with session() as c:
            self._contact_stm(c)

        return self._list_horaire

    def get_bus(self):
        return self._bus

    def _contact_stm(self, requests_session):

        time_format = '%Hh%M'
        current_time = datetime.datetime.now()

        try:
            if self._list_horaire is not None:
                del self._list_horaire[:]
            list_horaire = list()

            # Get URL content
            response_get = requests_session.get(
                self._url_site, headers=self._header)

            # In case of debug save the current web page
            # if self._logger.isEnabledFor(logging.DEBUG):
            #    with open("web_page.html", "w") as f:
            #        f.write(response_get.text)

            # Convert to object web page
            soup = BeautifulSoup(response_get.text, "html.parser")

            # Extract all web items corresponding to values
            v = soup.find_all('div', {'class': ['arrivals-list-item-text']})
#             self._logger.debug("Json stm rt {}".format(v))
            for result in v:
                # Filter to extract only numeric values in a new list
                string_val = result.string
                if string_val is not None:
                    time_extracted = string_val.replace("\n", "").strip()
                    self._logger.debug(
                        "Bus {} - Time:{}".format(self._bus, time_extracted))
                    list_split = time_extracted.split()
                    if len(list_split) == 2:
                        time_number = time_extracted.split()[0]
                        if time_number.isdigit():
                            time_value = int(time_number)
                            list_horaire.append(time_value)
                        elif time_number == "<1":
                            list_horaire.append(1)

                    elif len(list_split) == 3:
                        # Time is in hour and not time left
                        # By default is corresponding to the estimated time
                        # available by Database
                        # Code after permit to convert from string into
                        # minutes left
                        try:
                            time_concatenated = time_extracted.replace(
                                " ", "")
                            extracted_time = datetime.datetime.strptime(
                                time_concatenated, time_format)
                            extracted_time.replace(
                                day=current_time.day, month=current_time.month, year=current_time.year)
                            time_value = int(
                                (extracted_time - current_time).seconds / 60)
                            list_horaire.append(-time_value)
                        except ValueError:
                            # Ignore this error
                            pass
                    else:
                        # Add 0 to conserve order
                        list_horaire.append(0)
                        self._logger.error("Invalid value catched {}".format(list_split))

            self._list_horaire = list_horaire
        except Exception:
            self._logger.error("Error de connexion au serveur.")


if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG)
    manip = StmRealTime(False)

    manip.set_ligne_bus(51)
    manip.set_arret(51176)
    manip.set_nombre_horaire(4)
    manip.set_direction("West")

    list_prochain = manip.get_buses()
    print(list_prochain)
