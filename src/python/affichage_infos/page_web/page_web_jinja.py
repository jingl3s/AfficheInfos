# -*- coding: latin-1 -*-
"""
@author: jingl3s

"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import jinja2
import logging
import os
import shutil
import subprocess

from affichage_infos.interface_affiche_infos import (
    InterfaceAfficheInfos,
    InterfaceAfficheHorairesBus,
    InterfaceAfficheTemperatures,
)
from donnees.horaire_stm.info_bus import InformationBus


class PageWebLocale(InterfaceAfficheInfos, InterfaceAfficheHorairesBus, InterfaceAfficheTemperatures):
    """
    Construction affichage pour les bus
    """

    BASE_NOM_FICHIER = "infos"
    FICHIER_SORTIE = BASE_NOM_FICHIER + ".html"
    DOSSIER_RESSOURCES = "resources"
    DOSSIER_RSRC_FICHIERS = "web"
    DOSSIER_TPL = "tpl"
    TPL_FILE = "infos_bus.html.jinja2"
    #     TPL_FILE = "infos.html.jinja2"
    DOSSIER_CIBLE = "/tmp/"
    NOMBRE_HORAIRE_BUS = 4

    def __init__(self):
        """
        RAS
        """
        # External resources referenced by template
        self._chemin_resources = os.path.abspath(os.path.dirname(__file__)) + os.path.sep + self.DOSSIER_RESSOURCES
        # Template access
        self._tpl_path = os.path.abspath(os.path.dirname(__file__)) + os.path.sep + self.DOSSIER_TPL
        self._heure_courante = 0
        self._affiche_navigateur = False
        self._action_copie_fichiers = False
        self._logger = logging.getLogger(self.__class__.__name__)
        self._dossier_cible = self.DOSSIER_CIBLE
        self._dernier_extinction_ecran = None
        self._list_temperatures = list()
        self._list_previsions = list()
        self._list_previsions_semaine = list()
        self._list_taux = list()
        self._data = dict()
        self._extinction_ecran = True
        self._nom_dossier_ressources_utiliser = ""
        self._horaire_bus_multi = list()

    def set_dossier_cible(self, p_chemin_sortie):
        self._dossier_cible = p_chemin_sortie
        self._logger.debug("New output files directory {}".format(self._dossier_cible))

    def set_chemin_resources(self, p_chemin: str):
        self._chemin_resources = p_chemin

    def set_nom_dossier_ressources_utiliser(self, p_dossier: str):
        self._nom_dossier_ressources_utiliser = p_dossier

    def set_config(self, p_config):
        pass

    def set_dict_temperatures(self, p_dict_temperature):
        """

        :param p_dict_temperature:
                    ex : {"TEMP1":[6, datetime.datetime], "TEMP2":[6, datetime.datetime]} 
        """
        self._prepare_elements_tableau_temperature(p_dict_temperature)

    def set_dict_senseur(self, p_valeurs: dict, p_index: int = 0):
        """
        Prend le dictionnaire et le converti dans le format interne pour le processus d'affichage

        :param p_horaires:
        """
        #        self._prepare_elements_tableau_horaire_multi(p_horaires, p_index)
        pass

    def set_previsions(self, p_list_previsions):
        """

        :param p_list_previsions:
                    ex : [ {prevision1}, {prevision2} ] 
        """
        self._list_previsions = p_list_previsions

    def set_previsions_semaine(self, p_list_previsions_semaine):
        """

        :param p_list_previsions:
                    ex : [ {prevision1}, {prevision2} ] 
        """
        self._list_previsions_semaine = p_list_previsions_semaine

    def set_dict_horaires_multi(self, p_horaires: dict, p_index: int = 0):
        """
        Prend le dictionnaire et le converti dans le format interne pour le processus d'affichage

        :param p_horaires:
            ex : {TITRE: 'Snowdon', 'HORAIRE':[ InformationBus:Object1, InformationBus:Object2 ]}
        """
        self._prepare_elements_tableau_horaire_multi(p_horaires, p_index)

    def set_extinction_ecran(self, p_eteindre_ecran):
        self._extinction_ecran = p_eteindre_ecran

    def set_taux(self, p_list_taux):
        self._list_taux = p_list_taux

    def set_data_with_key(self, p_valus: [dict, str], p_key: str):
        self._data[p_key] = p_valus

    def affiche_obtenir_contenu(self, dt_current_date_time):
        self._heure_courante = dt_current_date_time

        output_text = self._set_jinja_info()

        return output_text

    def affiche_information(self, dt_current_date_time):

        output_text = self.affiche_obtenir_contenu(dt_current_date_time)

        with open(os.path.join(self._dossier_cible, self.FICHIER_SORTIE), "w") as fichier:
            fichier.write(output_text)
        if not self._affiche_navigateur:
            self._copie_fichiers()
            try:
                exec_return = subprocess.run(["xdg-open", os.path.join(self._dossier_cible, self.FICHIER_SORTIE), "&"])
            except FileNotFoundError:
                # ignore any error but display the file in the console to help
                self._logger.info(
                    f"Execution xdg-open fails for {os.path.join(self._dossier_cible, self.FICHIER_SORTIE)}"
                )

            self._affiche_navigateur = True

    def _copie_fichiers(self):
        if not self._action_copie_fichiers:
            self._logger.debug("Premier affichage, copie des fichiers")

            dossier_fichiers = os.path.join(self._dossier_cible, self._nom_dossier_ressources_utiliser)

            if not os.path.exists(dossier_fichiers):
                os.mkdir(dossier_fichiers)

            dossier_rsrc = os.path.join(self._chemin_resources, self.DOSSIER_RSRC_FICHIERS)

            for fichier in os.listdir(path=dossier_rsrc):
                dest_file = os.path.join(self._dossier_cible, dossier_fichiers, fichier)
                src_file = os.path.join(dossier_rsrc, fichier)

                if not os.path.exists(dest_file) or (os.path.getmtime(dest_file) < os.path.getmtime(src_file)):
                    shutil.copy(src_file, dest_file)

            self._action_copie_fichiers = True
        return self._action_copie_fichiers

    def _set_jinja_info(self):
        """
        source : http://kagerato.net/articles/software/libraries/jinja-quickstart.html
        :param dict_horaires_bus:
        """

        # In this case, we will load templates off the filesystem.
        # This means we must construct a FileSystemLoader object.
        #
        # The search path can be used to make finding templates by
        #   relative paths much easier.  In this case, we are using
        #   absolute paths and thus set it to the filesystem root.
        templateLoader = jinja2.FileSystemLoader(searchpath=self._tpl_path)

        # An environment provides the data necessary to read and
        #   parse our templates.  We pass in the loader object here.
        templateEnv = jinja2.Environment(loader=templateLoader)

        # This constant string specifies the template file we will use.
        TEMPLATE_FILE = self.TPL_FILE

        # Read the template file using the environment object.
        # This also constructs our Template object.
        template = templateEnv.get_template(TEMPLATE_FILE)

        dt_current_date_time = datetime.datetime.now()

        # Specify any input variables to the template as a dictionary.
        templateVars = {"date_creation_fichier": dt_current_date_time.strftime("%d/%m/%Y %H:%M")}

        if len(self._list_temperatures) > 0:
            templateVars["table_temperatures"] = self._list_temperatures

        if len(self._horaire_bus_multi) > 0:
            templateVars["table_horaires_multi"] = self._horaire_bus_multi

        if self._list_previsions is not None and len(self._list_previsions) > 0:
            templateVars["table_previsions"] = self._list_previsions

        if self._list_previsions_semaine is not None and len(self._list_previsions_semaine) > 0:
            templateVars["table_previsions_semaine"] = self._list_previsions_semaine
        if self._list_taux is not None and len(self._list_taux) > 0:
            templateVars["table_taux"] = self._list_taux

        # Add / at end to ensure is available and avoid error in case of empty
        # string
        templateVars["dossier_rsrc_server"] = self._nom_dossier_ressources_utiliser
        if not self._nom_dossier_ressources_utiliser == "":
            templateVars["dossier_rsrc_server"] += "/"

        # Add other parameters already formated
        for key, values in self._data.items():
            templateVars[key] = values

        # Finally, process the template to produce our final text.
        output_text = template.render(templateVars)

        return output_text

    def eteindre_ecran(self, dt_heure_courante):
        """
        Source: 
        http://askubuntu.com/questions/216783/ubuntu-12-10-turn-screen-off-when-inactive-for-never-still-turns-off
        https://www.kirsle.net/blog/entry/turn-off-monitor-from-linux-cli
        :param dt_heure_courante:
        """
        if self._extinction_ecran:

            if self._dernier_extinction_ecran is None:
                self._dernier_extinction_ecran = dt_heure_courante

            # Calcul difference en secondes
            #         self._logger.debug("Avant extinction ecran")
            calcul = ((dt_heure_courante - self._dernier_extinction_ecran).seconds) / 60.0
            #         self._logger.debug("Avant extinction ecran, calcul: '{0}'".format(calcul))
            if calcul > 10:
                self._logger.debug("Extinction �cran")
                os.system("xset dpms force off")
                self._logger.debug("Extinction �cran : fin")
                self._dernier_extinction_ecran = dt_heure_courante

    def _prepare_elements_tableau_horaire_multi(self, p_horaire, p_index):

        # Create default table if index does not exist
        if p_index >= len(self._horaire_bus_multi):
            for _ in range(len(self._horaire_bus_multi), p_index + 1):
                self._horaire_bus_multi.append(dict())

        # Fill it with content
        if len(p_horaire) > 0:
            self._horaire_bus_multi[p_index]["TITRE"] = p_horaire["TITRE"]
            self._horaire_bus_multi[p_index]["HORAIRE"] = p_horaire["HORAIRE"][: self.NOMBRE_HORAIRE_BUS]

    def _prepare_elements_tableau_temperature(self, dict_temperature):

        if len(dict_temperature) > 0:
            del self._list_temperatures[:]
            for capteur in sorted(dict_temperature):
                if dict_temperature[capteur][0] is not None:
                    temperature_loc = dict_temperature[capteur][0]
                    try:
                        temperature = "{:0.1f}".format(float(str(temperature_loc).split(" ")[0]))
                    except ValueError:
                        temperature = "N/A"

                    horaire_formate = dict_temperature[capteur][1].strftime("%d/%m/%Y %H:%M")

                    self._list_temperatures.append([capteur, horaire_formate, temperature])


#                     self._logger.debug('{:0.1f}'.format(float(str(temperature).split(' ')[0])))


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    # logging.getLogger().setLevel(logging.DEBUG)

    page = PageWebLocale()
    page.set_chemin_resources(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "resources",)))

    if not os.path.exists("/tmp"):
        page.set_dossier_cible(os.path.dirname(__file__))
    dict_temperatures = dict()
    dict_temperatures["T_EXT"] = [6, datetime.datetime.now()]
    dict_temperatures["T_INT"] = [23, datetime.datetime.now()]
    page.set_dict_temperatures(dict_temperatures)

    list_horaires_bus = list()
    list_horaires_bus.append(InformationBus(51, "21:23", "10", "12"))
    list_horaires_bus.append(InformationBus(51, "21:43", "5", "6"))
    list_horaires_bus.append(InformationBus(51, "21:55", "5", "7"))

    page.set_dict_horaires_multi({"TITRE": "Snowdon", "HORAIRE": list_horaires_bus}, 0)
    page.set_dict_horaires_multi({"TITRE": "Oratoire", "HORAIRE": list_horaires_bus}, 1)
    # page.set_dict_horaires_multi({"TITRE": "Destination2", "HORAIRE": list_horaires_bus}, 2)

    list_prevision = [
        {
            "humidite": "88",
            "heure": "12",
            "condition": "Neige",
            "temperature": "-3",
            "icon": "http://icons.wxug.com/i/c/k/snow.gif",
        },
        {
            "humidite": "88",
            "heure": "15",
            "condition": "Neige",
            "temperature": "-1",
            "icon": "http://icons.wxug.com/i/c/k/snow.gif",
        },
        {
            "humidite": "91",
            "heure": "18",
            "condition": "Neige",
            "temperature": "0",
            "icon": "http://icons.wxug.com/i/c/k/nt_snow.gif",
        },
        {
            "humidite": "93",
            "heure": "21",
            "condition": "Averses de neige",
            "temperature": "1",
            "icon": "http://icons.wxug.com/i/c/k/nt_snow.gif",
        },
        {
            "humidite": "95",
            "heure": "00",
            "condition": "Averses de neige",
            "temperature": "0",
            "icon": "http://icons.wxug.com/i/c/k/nt_snow.gif",
        },
        {
            "humidite": "87",
            "heure": "03",
            "condition": "Couvert",
            "temperature": "-1",
            "icon": "http://icons.wxug.com/i/c/k/nt_cloudy.gif",
        },
        {
            "humidite": "83",
            "heure": "06",
            "condition": "Couvert",
            "temperature": "-2",
            "icon": "http://icons.wxug.com/i/c/k/nt_cloudy.gif",
        },
        {
            "humidite": "82",
            "heure": "09",
            "condition": "Couvert",
            "temperature": "-2",
            "icon": "http://icons.wxug.com/i/c/k/cloudy.gif",
        },
    ]
    page.set_previsions(list_prevision)
    page.affiche_information(datetime.datetime.now())
