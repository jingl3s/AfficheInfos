# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import requests
from requests.exceptions import Timeout
import logging


class RequestURL(object):
    '''
    Access to a URL in order to retrieve information 
    Manage the timeout as list in order to have a management in case of server taking more time to answer
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self._logger = logging.getLogger(self.__class__.__name__)
        self._time_out_values = [3.0, 10.0]
        self._time_out = self._time_out_values[0]
        self._enable_retry = True
        self._url = None

    def set_url(self, p_url: str):
        self._url = p_url

    def set_enable_retries(self, p_enable):
        self._enable_retry = p_enable

    def get_url_request(self):
        timeout_counter = -1
        loop_retries = True
        return_value = None
        response = None
        while loop_retries:
            try:
                response = requests.get(self._url, timeout=self._time_out)
                return_value = response.text
            except Timeout as e:
                timeout_counter += 1
                if timeout_counter >= len(self._time_out_values) or not self._enable_retry:
                    loop_retries = False
                else:
                    self._time_out = self._time_out_values[timeout_counter]
                    self._logger.debug(
                        "New timer enabled {}".format(self._time_out))
            except Exception as e:
                raise Exception(e)
            else:
                loop_retries = False
            finally:
                if response is not None:
                    response.close()

        return return_value
