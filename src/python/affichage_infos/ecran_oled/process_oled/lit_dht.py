# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).
import logging
from random import randint


class DHTFake(object):
    def __init__(self, sensor):
        self._sensor = sensor

    def get_valeurs(self):
        self.valeurs = (randint(-200, 300) / 10.0, randint(100, 950) / 10.0)
        return self.valeurs

    def get_sensor(self):
        return self._sensor


class ReadDht():
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._sensors = list()
        
        self._create_dht()

    def _create_dht(self):
        
        try:
            from affichage_infos.ecran_oled.process_oled.dht.dht_interface import DHTInterface
            dht11 = DHTInterface(0, 11)
            dht22 = DHTInterface(1, 22)
            self._logger.info("DHT reel")
        except:
            if self._logger.isEnabledFor(logging.DEBUG):
                dht11 = DHTFake(11)
                dht22 = DHTFake(22)
                self._logger.info("DHT fake")

        self._sensors.append(dht11)
        self._sensors.append(dht22)

    def get_values(self):
        values = list()
        for sensor in self._sensors:
            values.append(self._get_dht(sensor))
        return ",".join(values)

    def _get_dht(self, dht_sensor):
        temp, hum = dht_sensor.get_valeurs()
#         sensor = dht_sensor.get_sensor()
#        return '{}:{:2.0f}/{:2.0f}%,'.format(sensor/10,
        return '{:2.0f}/{:2.0f}%'.format(
            temp, hum)
